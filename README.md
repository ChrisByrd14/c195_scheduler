# C195 Scheduler Application

## About this project

The Primary Key of each database table used in this project were altered by setting them to auto-increment.

This project was built using automated testing from jUnit.

The following JAR files are bundled in this project at the project root directory:

 * MySQL Connector
 * jUnit
 * Hamcrest (a jUnit dependency)

To run the test suite:

 * On Windows, press: `Alt + F6`
 * On macOS, press: `^ + F6`
 * Or, in the menu bar, click: `Run -> Test Project`


## Assumptions made in/Information about this application:

* Data validation assumptions:
	* Valid phone numbers are 10 numeric digits long (non-numeric digits are stripped from the fields via RegEx)
	* Valid postal codes are 5 numeric digits long (non-numeric digits are stripped from the fields via RegEx)
	* Valid meeting duration is entered in HH:mm format, where hours and minutes are separated by a colon. Negative values are considered invalid and will return an error.
	* Meeting URLs are required and must be able to be reached by Java's URLConnection library. You must include either http:// or https:// in the URL.
	* Appointments cannot be scheduled to start outside of 8am to 5pm Monday through Friday. Appointments are, however, allowed to carry over after business hours. So, scheduling an appointment at 4:55 to last for a time greater than 5 minutes IS allowed, but scheduling an appointment to start at 7:59am IS NOT allowed.
	* Customer contact is presumed to be a name or phone number, but the only validation on the customer contact is checking that the field is not empty, to give the user freedom to use that field as they see fit.
* The month-based and week-based calendars operate independently of each other since some users may use the two calendars for different tasks (i.e. using the month-based calendar to get a quick overview of how next month looks, while using the week-calendar for appointment management).
* When creating/updating appointments, users are presented with a list of only customers that are marked as active in the database.
* Deleting appointments can be done by clicking the green appointment pane on the calendar and clicking the "Delete" button on the Appointment Detail view.

## User list:

Username |   Plaintext Password  | Hashed Password
:------- | :-------------------- | :------------------------------------------------
test     |        test           | 37268335dd693145bdcdf92623ff819a64244b53de746d438
user     |      Pa$$w0rd!        | bb8834c5b3467b48fbd24e2c436c41b6f2e18d17d7a4ff1ad
jdoe     |   aSimplePassword     | 1abccf6524d4fab394487ab8c117cac295f4975971aa21e14
test1    | anotherSimplePassword | 25f7ae79b23180cc966ecf23128264c84eb5f7dcb9c4204ee


## Project Requirement Details:

1. A. Uncomment first line of code in `LoginController.initialize()` to change default language to Spanish
2. B. Customers may be added by clicking `New->Customer...` on the main application screen or the menu bar at the top of the main screen. Updating customers can be done by clicking `Update->Customer...` on the menu bar of the main screen.
3. C. Lambdas are used in the `AppointmentValidator` class for validating provided appointment data. Adding appointments can be done by clicking `New->Appointment...` or the `New Appointment` button on the Main Screen. To view appointment details, click the green band on a day in the calendar. To edit that appointment click the `Edit` button on the Appointment Detail screen.
4. D. Implemented using the `AppointmentCalendar`, `AbstractCalendar`, `WeekCalendar`, and `MonthCalendar` classes.
5. E. Appointment times are converted to UTC prior to being inserted into the database. Times are converted from UTC to the user's system's default TimeZone in `AbstractCalendar`, `AppointmentDetailController`, and `EditAppointmentController` 
6. F. `Try/Catch` used throughout the application. `Try-with-Resources` used in `LoginAuditor.writeToLog()`.
7. G. Implemented in `BaseController.presentClosePrompt()`
8. H. Implemented in `MainScreenController.initData()` 
9. I. Can be ran from the "Reports" menu item on the menu bar. Reports can be save to a CSV file on the host computer.
10. J.  The login log file will be created in /src/scheduler/ and appended to with each attempted login.
