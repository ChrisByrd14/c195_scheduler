/**
 * Wraps the data returned as an error for failed data validations
 */

package scheduler;

public class ValidationError {
    /**
     * Message to present to the user
     */
    private String message;

    /**
     * FormElement object that failed validation
     */
    private Object element;

    public ValidationError(Object element, String message) {
        this.element = element;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getElement() {
        return element;
    }

    public void setElement(Object element) {
        this.element = element;
    }
}
