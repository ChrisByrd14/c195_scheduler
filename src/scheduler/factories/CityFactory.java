package scheduler.factories;

import java.util.ArrayList;
import java.util.Iterator;
import scheduler.FormElement;
import scheduler.models.City;

public class CityFactory {

    protected City model = new City();
    protected String cityName;
    protected int countryID = -1;
    protected int userID = -1;

    /**
     * Check if a city/country combo already exists in the database. If it doesn't then create it and return it.
     * @param cityElement Form element of the city to be retrieved/created
     * @param countryID id of the related country table record
     * @param userID user id that is creating this city, if we need to create it
     * @return City object of the retrieved/created record
     */
    public City getOrAdd(FormElement cityElement, int countryID, int userID) {
        this.countryID = countryID;

        this.cityName = ((String) cityElement.getValue()).trim();
        this.userID = userID;

        City city = this.getAndNarrowDownCities();
        if (city != null && city.getID() > 0)
            return city;

        int result = this.model.insertNewCity(userID, this.cityName, this.countryID);
        if (result < 0) {
            // Database issue, return null
            return null;
        }

        return this.getAndNarrowDownCities();
    }

    /**
     * Query database for any matches with the given city name, and search the returned value for the correct record
     * @param cityName Search value used in the SELECT query
     */
    private City getAndNarrowDownCities() {
        ArrayList<City> cities = this.model.getByName(this.cityName);

        Iterator it = cities.iterator();
        while (it.hasNext()) {
            City currentCity = (City) it.next();
            if (currentCity.getCountryID() == this.countryID)
                return currentCity;
        }
        return null;
    }
}
