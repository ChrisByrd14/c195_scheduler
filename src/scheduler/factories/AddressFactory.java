package scheduler.factories;

import scheduler.FormElement;
import scheduler.models.Address;

public class AddressFactory {
    protected Address model = new Address();
    protected Address desiredAddress;
    protected String address1;
    protected String address2;
    protected String phone;
    protected String postalCode;
    protected int cityID;
    protected int userID = -1;

    /**
     * Check if the address exists, if not add it to database
     * @param addressElement1 FormElement containing address 1 text field
     * @param addressElement2 FormElement containing address 2 text field
     * @param addressCity FormElement containing city text field
     * @param postalCodeElement FormElement containing postal code text field
     * @param phoneElement FormElement containing phone text field
     * @param userID ID of the user creating the address
     * @return 
     */
    public Address getOrAdd(FormElement addressElement1, FormElement addressElement2, int addressCity,
            FormElement postalCodeElement, FormElement phoneElement, int userID) {

        this.cityID = addressCity;
        this.address1 = ((String) addressElement1.getValue()).trim();
        this.address2 = ((String) addressElement2.getValue()).trim();
        this.postalCode = ((String) postalCodeElement.getValue()).trim().replaceAll("[^\\d]", "");
        this.phone = ((String) phoneElement.getValue()).trim().replaceAll("[^\\d]", "");
        this.userID = userID;

        Address address = this.getAddress();
        if (address != null && address.getID() > 0)
            return address;

        // Address not in database, create it
        this.insertAddress();

        address = this.getAddress();
        if (address != null && address.getID() > 0)
            return address;

        return null;
    }

    /**
     * Find and retrieve address from the database
     * @return Address object
     */
    protected Address getAddress() {
        return this.model.findAddress(this.address1, this.address2, this.cityID,
               this.postalCode, this.phone);
    }

    /**
     * Insert address into the database
     */
    protected void insertAddress() {
        this.model.insert(this.address1, this.address2, this.cityID,
                this.postalCode, this.phone, this.userID);
    }
}
