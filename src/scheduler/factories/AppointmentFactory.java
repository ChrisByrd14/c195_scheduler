package scheduler.factories;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import javafx.collections.ObservableList;
import scheduler.FormElement;
import scheduler.models.Appointment;
import scheduler.models.AppointmentType;
import scheduler.models.EditCustomer;
import scheduler.models.User;

public class AppointmentFactory {
    private Appointment model;
    private HashMap<String, FormElement> fields;

    public AppointmentFactory() {
        this.model = new Appointment();
    }

    /**
     * Create a new appointment record in the database
     * @param fields HashMap containing all FormElements from the controller
     * @param user logged-in user
     * @return true if record is successfully inserted
     */
    public boolean createNewAppointment(HashMap<String, FormElement> fields, User user) {
        this.fields = fields;
        String start = this.getStartInUTC(
                (LocalDate) this.fields.get("date").getValue(), (String) this.fields.get("startTime").getValue());
        String end = this.getEndInUTC(start, (String) this.fields.get("duration").getValue());

        return this.model.createNewAppointment(
                ((EditCustomer) this.fields.get("customer").getValue()).getID(),
                (String) this.fields.get("title").getValue(),
                (String) this.fields.get("description").getValue(),
                (String) this.fields.get("location").getValue(),
                (String) this.fields.get("contact").getValue(),
                (String) this.fields.get("url").getValue(),
                start,
                end,
                user.getID());
    }

    /**
     * Update appointment record in the database
     * @param fields HashMap containing all FormElements from the controller
     * @param user logged-in user
     * @return true if record is successfully updated
     */
    public boolean updateAppointment(HashMap<String, FormElement> fields, User user) {
        this.fields = fields;
        String start = this.getStartInUTC(
                (LocalDate) this.fields.get("date").getValue(), (String) this.fields.get("startTime").getValue());
        String end = this.getEndInUTC(start, (String) this.fields.get("duration").getValue());
        return this.model.updateAppointment(
                Integer.parseInt((String) this.fields.get("appointmentID").getValue()),
                ((EditCustomer) this.fields.get("customer").getValue()).getID(),
                (String) this.fields.get("title").getValue(),
                (String) this.fields.get("description").getValue(),
                (String) this.fields.get("location").getValue(),
                (String) this.fields.get("contact").getValue(),
                (String) this.fields.get("url").getValue(),
                start,
                end,
                user.getID());
    }

    /**
     * Retrieve all unique appointment types (titles) within a date range
     * @param minDate minimum date to check records for
     * @param maxDate maximum date to check records for
     * @return ObservableList of AppointmenType objects
     */
    public ObservableList<AppointmentType> getAppointmentTypes(String minDate, String maxDate) {
        return this.model.getAppointmentTypes(minDate, maxDate);
    }

    /**
     * Check for any overlapping appointment records in the database
     * @param user logged-in user
     * @param fields HashMap of FormElements from the controller
     * @param appointmentID ID of the appointment to ignore.
     * @return count of overlapping appointments
     */
    public int checkOverlappingAppointments(User user, HashMap<String, FormElement> fields, int appointmentID) {
        String start = this.getStartInUTC(
                (LocalDate) fields.get("date").getValue(), (String) fields.get("startTime").getValue());
        String end = this.getEndInUTC(start, (String) fields.get("duration").getValue());
        return this.model.getNumberOfOverlappingAppointments(user, start, end, appointmentID);
    }

    /**
     * Retrieve all appointments ordered by consultant
     * @return ObservableList of Appointment objects
     */
    public ObservableList<Appointment> getAllAppointmentsOrderedByUser() {
        return this.model.getAllAppointmentsOrderedByUser();
    }

    /**
     * Get all upcoming appointments in the next 15 minutes.
     * @param user logged-in user to check the appointments of
     * @param invalidList list of all appointment IDs to ignore
     * @return ArrayList of Appointment objects for upcoming appointments
     */
    public ArrayList<Appointment> getUpcomingAppointments(User user, ArrayList<Integer> invalidList) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return this.model.getUpcomingAppointments(user, simpleDateFormat.format(calendar.getTime()), invalidList);
    }

    /**
     * Convert date and time to UTC
     * @param date LocalDate from the form's datepicker
     * @param time time as entered by the user
     * @return UTC timestamp string
     */
    protected  String getStartInUTC(LocalDate date, String time) {
        String[] times = time.split(":");
        ZonedDateTime dateTime = ZonedDateTime.of(date,
                LocalTime.of(Integer.parseInt(times[0]), Integer.parseInt(times[1])), ZoneId.systemDefault());
        String utcDateTime = dateTime.toInstant().toString();
        return  utcDateTime.replace("T", " ").replace("Z", "");
    }

    /**
     * Convert the duration and start dateString to the "end" UTC timestamp
     * @param dateString UTC date string
     * @param time duration
     * @return UTC timestamp for end column
     */
    protected String getEndInUTC(String dateString, String time) {
        String[] times = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.convertUTCStringToDate(dateString));
        calendar.add(Calendar.HOUR, Integer.parseInt(times[0]));
        calendar.add(Calendar.MINUTE, Integer.parseInt(times[1]));
        Date date = calendar.getTime();
        return date.toInstant().toString().replace("T", " ").replace("Z", "");
    }

    /**
     * Convert UTC string timestamp to Date object
     * @param utcDate string representation of the UTC timestamp
     * @return Date object
     */
    private Date convertUTCStringToDate(String utcDate) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(tz);

        try {
            return formatter.parse(utcDate);
        } catch (Exception e) {
            return null;
        }
    }
}
