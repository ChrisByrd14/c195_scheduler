package scheduler.factories;

import java.util.HashMap;
import scheduler.FormElement;
import scheduler.models.User;

public class UserFactory {
    protected User model = new User();

    /**
     * Load a user from the database
     * @param fields Form fields
     * @return User object on success, null on failure
     */
    public User loadFromDB(HashMap<String, FormElement> fields) {
        return this.model.tryLogin((String) fields.get("userName").getValue(),
                (String) fields.get("password").getValue());
    }
}
