package scheduler.factories;

import scheduler.models.Country;

public class CountryFactory {

    /**
     * Returns the value from the ID column of the country table in the database
     * @param countryName Name of the country to search the database for
     * @return integer representing the ID of the country in the database
     */
    public static int getIDByName(String countryName) {
        Country model = new Country();
        return model.getCountryID(countryName.trim());
    }
}
