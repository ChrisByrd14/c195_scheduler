package scheduler.factories;

import javafx.collections.ObservableList;
import scheduler.CustomerStatus;
import scheduler.FormElement;
import scheduler.models.Customer;
import scheduler.models.EditCustomer;

public class CustomerFactory {

    protected Customer model = new Customer();

    /**
     * Create the necessary records to insert a customer into the database.
     * @param customerName Form element for the customer's name
     * @param status Form element for the customer status
     * @param addressID ID of the related record in the address table
     * @param userID ID of the user that is creating the customer
     * @return true if customer was created, else false
     */
    public boolean createCustomer(FormElement customerName, FormElement status,
            int addressID, int userID) {

        return this.model.insert(
                (String) customerName.getValue(),
                addressID,
                this.getActiveStatus((CustomerStatus) status.getValue()),
                userID
        );
    }

    /**
     * Return an ArrayList of all customers
     * @return ArrayList of all customers if successful, else null
     */
    public ObservableList<EditCustomer> getAllCustomers() {
        EditCustomer model = new EditCustomer();
        return model.getAllCustomers();
    }

    public ObservableList<EditCustomer> getAllActiveCustomers() {
        EditCustomer model = new EditCustomer();
        return model.getAllActiveCustomers();
    }

    public boolean updateCustomer(int customerID, FormElement customerName, FormElement status,
            int addressID, int userID) {

        return this.model.update(
                customerID,
                (String) customerName.getValue(),
                addressID,
                this.getActiveStatus((CustomerStatus) status.getValue()),
                userID
        );
    }

    private int getActiveStatus(CustomerStatus status) {
        return (status == CustomerStatus.ACTIVE ? 1 : 0);
    }

    public Customer getCustomerByID(int customerID) {
        return this.model.getByID(customerID);
    }
}
