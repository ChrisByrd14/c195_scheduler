package scheduler;

import java.util.Calendar;
import java.util.TimeZone;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import scheduler.controllers.MainScreenController;
import scheduler.models.Appointment;
import scheduler.models.User;

public class WeekCalendar extends AbstractCalendar implements ICalendar {

    public WeekCalendar(User user, MainScreenController controller) {
        super(user, controller);
        this.calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
    }

    /**
     * Generate and return the week-based calendar of appointments
     * @return VBox representing the generated calendar
     */
    public VBox getCalendarOfAppointments() {
        this.clearView();

        StringBuilder labelText = new StringBuilder();
        this.resetWeek();

        int firstDay = this.calendar.get(Calendar.DAY_OF_MONTH);

        labelText.append(this.months[this.calendar.get(Calendar.MONTH)]);
        labelText.append(" ");
        labelText.append(this.calendar.get(Calendar.DAY_OF_MONTH));
        labelText.append(" ");
        labelText.append(this.calendar.get(Calendar.YEAR));

        labelText.append(" - ");

        this.calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);

        int lastDay = this.calendar.get(Calendar.DAY_OF_MONTH);

        labelText.append(this.months[this.calendar.get(Calendar.MONTH)]);
        labelText.append(" ");
        labelText.append(this.calendar.get(Calendar.DAY_OF_MONTH));
        labelText.append(" ");
        labelText.append(this.calendar.get(Calendar.YEAR));

        Label weekLabel = new Label(labelText.toString());
        this.addToView(weekLabel);

        this.loadDays();

        this.resetWeek();
        this.generateWeek(firstDay, lastDay);

        // calendar is iterated through week, reset it
        this.resetWeek();
        return this.view;
    }

    /**
     * Get and return a week-based calendar for the previous week
     * @return VBox with previous week's calendar
     */
    public VBox getPrevious() {
        this.calendar.add(Calendar.WEEK_OF_YEAR, -2);
        return this.getCalendarOfAppointments();
    }

    /**
     * Get and return a week-based calendar for the next week
     * @return VBox with next week's calendar
     */
    public VBox getNext() {
        return this.getCalendarOfAppointments();
    }

    /**
     * Generate the rest of the week
     * @param dayNumber number of the day of the month
     * @param lastDay [unused]
     */
    @Override
    protected void generateWeek(int dayNumber, int lastDay) {
        HBox week = new HBox();
        for (int weekDayNumber = 0; weekDayNumber < 7; weekDayNumber++) {
            week.getChildren().add(this.getDay(dayNumber));
            this.calendar.add(Calendar.DAY_OF_WEEK, 1);
            dayNumber++;
        }

        this.resetWeek();
        this.view.getChildren().add(week);
    }

    /**
     * Check start time agains the this.calendar. If dates match return true
     * @param appointment Appointment object to check start date of
     * @param dayNumber [unused]
     * @return true if appointment is on the current day that this.calendar is on
     */
    @Override
    protected boolean appointmentOnDay(Appointment appointment, int dayNumber) {
        TimeZone zone = TimeZone.getDefault();

        Calendar conversionCalendar = Calendar.getInstance();
        conversionCalendar.setTimeInMillis(appointment.getStart().getTime());
        conversionCalendar.add(Calendar.MILLISECOND, zone.getOffset(conversionCalendar.getTimeInMillis()));

        return (conversionCalendar.get(Calendar.YEAR) == this.calendar.get(Calendar.YEAR) &&
                conversionCalendar.get(Calendar.MONTH) == this.calendar.get(Calendar.MONTH) &&
                conversionCalendar.get(Calendar.DAY_OF_MONTH) == this.calendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Set this.calendar to Sunday of the current week
     */
    private void resetWeek() {
        this.calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
    }
}
