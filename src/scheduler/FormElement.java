/*
 * Wrapper class for JavaFX form elements.
 * Contains the element's human-readable name, the actual JavaFX object, and the actual value of the element
 */

package scheduler;

import javafx.scene.control.Control;

public class FormElement {
    private Control element;
    private Object value;
    private String name;

    public FormElement(String name, Control element, Object value) {
        this.element = element;
        this.name = name;
        this.value = value;
    }

    public Control getElement() {
        return element;
    }

    public void setElement(Control element) {
        this.element = element;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
