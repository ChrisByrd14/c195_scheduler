/**
 * Interface for calendar objects that display to the main screen.
 */
package scheduler;

import javafx.scene.layout.VBox;

public interface ICalendar {
    public VBox getNext();
    public VBox getPrevious();
    public VBox getCalendarOfAppointments();
}
