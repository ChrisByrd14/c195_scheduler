package scheduler.services;

import java.util.ArrayList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import scheduler.factories.AppointmentFactory;
import scheduler.models.Appointment;
import scheduler.models.User;

public class NotificationService extends ScheduledService<ArrayList<Appointment>> {

    private final AppointmentFactory appointment;
    private final User user;
    private final ArrayList<Integer> alreadyNotified = new ArrayList<Integer>();

    public NotificationService(AppointmentFactory appointment, User user) {
        this.appointment = appointment;
        this.user = user;
    }

    @Override
    protected Task<ArrayList<Appointment>> createTask() {
        return new Task() {
            @Override
            protected ArrayList<Appointment> call() {
                return null;
            }
        };
    }

    public ArrayList<Appointment> getAppointments() {
        return this.appointment.getUpcomingAppointments(this.user, this.alreadyNotified);
    }

    public void addAppointmentToNotified(Appointment appointment) {
        this.alreadyNotified.add(appointment.getID());
    }
}
