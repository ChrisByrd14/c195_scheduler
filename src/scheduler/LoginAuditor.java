package scheduler;

import scheduler.models.LoginLog;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LoginAuditor {

    private String currentDir;
    private final String filePath = "/src/scheduler/login_logs.txt";
    private String userName;

    public LoginAuditor() {
        this.currentDir = System.getProperty("user.dir");
    }

    /**
     * Log that userName has successfully logged in
     * @param userName name of the user
     * @return true if log is successful
     */
    public boolean logSuccessfulLogin(String userName) {
        this.userName = userName;
        return this.writeToLog("User '" + this.userName + "' logged in successfully.");
    }

    /**
     * Log that userName failed to log in
     * @param userName name of the user
     * @return true if log is successful
     */
    public boolean logFailedLogin(String userName) {
        this.userName = userName;
        return this.writeToLog("User '" + this.userName + "' failed to log in.");
    }

    /**
     * Write to the login file in the following format:
     * YYYY-MM-DD hh:mm:ss :  [ message text here ]
     * @param logRecord Message text of the record
     * @return true on success, false on exception
     */
    private boolean writeToLog(String logRecord) {
        boolean status = false;
        String timestamp = LocalDateTime.now().toString().replace("T", " ").substring(0, 19);

        StringBuilder log = new StringBuilder();
        log.append(timestamp);
        log.append(" : ");
        log.append(logRecord);
        log.append("\n");

        try (
            BufferedWriter writer =
                    new BufferedWriter(new FileWriter(this.currentDir + this.filePath, true));
        ) {
           writer.write(log.toString());
            status = true;
        } catch (IOException e) {
            System.out.println("File Write Exception:\t" + e.getMessage());
            System.out.println("Unable to write:\t" + log.toString());
        }
        return status;
    }

    /**
     * Read log records from the log file and return as an ObservableList
     * @return ObservableList filled with LoginLog objects
     */
    public ObservableList<LoginLog> readLog() {

        String line = null;
        ObservableList<LoginLog> records = FXCollections.observableArrayList();
        try (
            BufferedReader reader =
                    new BufferedReader(new FileReader(this.currentDir + this.filePath));
        ) {
            while ((line = reader.readLine()) != null) {
                records.add(0, new LoginLog(line.split(" : ")));
            }
        } catch (Exception e) {
            System.out.println("File Read Exception:\t" + e.getMessage());
        }

        return records;
    }
}
