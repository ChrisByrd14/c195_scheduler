package scheduler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
    private Connection connection = null;

    // Databaes credentials. Don't change.
    private String driver = "com.mysql.jdbc.Driver";
    private String db = "U041Hs";
    private String url = "jdbc:mysql://52.206.157.109/" + db;
    private String user = "U041Hs";
    private String pass = "53688145455";

    public Database() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            System.out.println("Database Exception: " + e.getMessage());
        }
    }

    /**
     * Make connection to database
     * @return true if connection was made successfully
     */
    public boolean connect() {
        try {
            this.connection = DriverManager.getConnection(this.url, this.user, this.pass);
            return this.isConnected();
        } catch (SQLException e) {
            System.out.println("DB Connection Exception: " + e.getMessage());
        }
        return false;
    }

    /**
     * Determine if connection already exists to the database
     * @return true if connection already exists
     */
    public boolean isConnected() {
        try {
            return this.connection != null && !this.connection.isClosed();
        } catch (SQLException e) {
            System.out.println("Database.isConnected Exception: " + e);
            return false;
        }
    }

    /**
     * Create and return a prepared statement from the DB connection.
     * @param query String SQL command to prepare
     * @return PreparedStatement object generated from query
     */
    public PreparedStatement getPreparedStatement(String query) {
        try {
            return this.connection.prepareStatement(query);
        } catch (SQLException e) {
            System.out.println("SQLException Thrown:\t" + e);
            return null;
        }
    }

    /**
     * Get a statement object from the connection
     * @return Statement object if successful
     */
    public Statement getStatement() {
        try {
            return this.connection.createStatement();
        } catch (Exception e) {
            System.out.println("Statement Exception Thrown:\t" + e);
            return null;
        }
    }

    /**
     * Attempt to close the DB connection
     * @return true if connection closes as expected
     */
    public boolean closeConnection() {
        try {
            this.connection.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
