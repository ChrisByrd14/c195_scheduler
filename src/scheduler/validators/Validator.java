/**
 * Wrapper class for validation logic.
 * Used to validate fields from the different forms in the Scheduler application
 */

package scheduler.validators;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import scheduler.FormElement;
import scheduler.ValidationError;

public class Validator {

    protected String[] textFields;
    protected Map<String, FormElement> fields;
    protected ValidationError error;

    /**
     * Check if the provided string isn't empty.
     * @param text text to check
     * @return true if trimmed text isn't empty
     */
    public boolean checkStringNotEmpty(String text) {
        return ( ! ( text == null ||  text.trim().equals("")));
    }

    /**
     * Check the length of a string whose non-numeric characters have been stripped from it
     * @param text String to verify digit-only length
     * @param length expected length
     * @return true if the length of digit-only version of text is the same as length
     */
    public boolean checkDigitLength(String text, int length) {
        return (text != null && text.trim().replaceAll("[^\\d]", "").length() == length);
    }

    /**
     * Loads each validation service with associated form fields
     * @param fields ArraList consisting of all form fields
     * @return true if fields is not empty
     */
    public boolean load(Map<String, FormElement> fields) {
        if (fields == null || fields.isEmpty()) {
            return false;
        }
        this.fields = fields;
        return true;
    }

    /**
     * Verify that the element isn't null and the value for element isn't an empty string
     * @param element The element to test
     */
    protected void validateString(FormElement element) {
        if (element == null) {
            this.createError(null, "No element provided.");
            return;
        }

        if (element.getValue() == null || ! this.checkStringNotEmpty((String) element.getValue()))
            this.createError(element, element.getName() + " cannot be empty.");
    }

    private void validateURL() {
        FormElement urlElement = this.fields.get("url");
        URLConnection conn = null;
        try {
            URL url = new URL((String) urlElement.getValue());
            conn = url.openConnection();
            conn.connect();
        } catch (MalformedURLException e) {
            this.createError(urlElement, "Please provide a valid URL.");
        } catch (IOException e) {
            // connection couldn't be established, but URL is valid
        }
    }

    /**
     * Create an error if an error hasn't already been created
     * @param element FormElement associated with the error
     * @param message Message to present the user
     */
    public void createError(FormElement element, String message) {
        if (this.error == null) {
            this.error = new ValidationError(element, message);
        }
    }

    /**
     * Gets the first error that occurred in the data validation
     * @return Error object with the failed form element and error message
     */
    public ValidationError getError() {
        return this.error;
    }

    /**
     * Clears the validation service error instance variable
     */
    public void clearError() {
        this.error = null;
    }
}
