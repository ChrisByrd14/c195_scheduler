package scheduler.validators;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import scheduler.FormElement;
import scheduler.ValidationError;
import scheduler.models.EditCustomer;

public class AppointmentValidator extends Validator implements ValidatorInterface {

    protected String[] textFields = {"title", "description", "location", "startTime", "duration", "contact", "url"};

    public AppointmentValidator() {}

    @Override
    public boolean validate() {

        if (this.fields == null || this.fields.isEmpty()) {
            this.error = new ValidationError(null, "No information provided.");
            return false;
        }

        if (this.error != null)
            return true;

        // Requirement C
        Arrays.asList(this.textFields).forEach((key) -> {
            this.validateString((FormElement) this.fields.get(key));
        });

        this.checkCustomer();
        this.checkStartTime();
        this.checkDuration();

        if (this.error != null)
            return false;

        this.checkFutureDate();
        this.checkMeetingNotOnWeekend();
        this.checkMeetingInBusinessHours();
        this.checkURL();

        return this.error == null;
    }

    protected void checkCustomer() {
        FormElement element = this.fields.get("customer");
        EditCustomer customer = (EditCustomer) element.getValue();

        if (customer == null || customer.getID() < 1)
            this.createError(element, "No customer selected.");
    }

    protected void checkFutureDate() {
        LocalDate date = (LocalDate) this.fields.get("date").getValue();

        if (date == null) {
            this.createError(this.fields.get("date"), "You must select a meeting date.");
            return;
        }

        LocalDateTime now = LocalDateTime.now();

        try {
            String[] times = ((String) this.fields.get("startTime").getValue()).split(":");
            LocalTime apptTime = LocalTime.of(Integer.parseInt(times[0]), Integer.parseInt(times[1]));
            LocalDateTime apptDateTime = LocalDateTime.of(date, apptTime);

            if ( ! apptDateTime.isAfter(now)) {
                this.createError(this.fields.get("date"), "Selected date cannot be in the past.");
            }
        } catch (Exception e) {
            // pass, since validation issues from 
        }


    }

    protected void checkDuration() {
        FormElement element = this.fields.get("duration");
        String duration = (String) element.getValue();
        String[] durationArray = duration.split(":");

        if (durationArray.length != 2) {
            this.createError(element, "Invalid data provided for meeting duration.");
            return;
        }

        try {
            int hours = Integer.parseInt(durationArray[0]);
            if (hours < 0) {
                throw new Exception("Duration hours cannot be negative.");
            } else if (hours > 23) {
                throw new Exception("Duration hours cannot be greater than 23");
            }

            int minutes = Integer.parseInt(durationArray[1]);
            if (minutes < 0) {
                throw new Exception("Duration minutes cannot be negative.");
            } else if (minutes > 59) {
                throw new Exception("Duration minutes cannot be greater than 59");
            }
        } catch (NumberFormatException e) {
            this.createError(element, "Meeting duration is invalid.");
        } catch (Exception e) {
            this.createError(element, e.getMessage());
        }
    }

    protected void checkStartTime() {
        FormElement element = this.fields.get("startTime");
        String duration = (String) element.getValue();
        String[] durationArray = duration.split(":");

        if (durationArray.length != 2) {
            this.createError(element, "Invalid data provided for meeting start time.");
            return;
        }

        try {
            int hours = Integer.parseInt(durationArray[0]);
            if (hours < 0) {
                throw new Exception("Start time hour cannot be negative.");
            }

            int minutes = Integer.parseInt(durationArray[1]);
            if (minutes < 0) {
                throw new Exception("Start time minutes cannot be negative.");
            }
        } catch (NumberFormatException e) {
            this.createError(element, "Meeting start time is invalid.");
        } catch (Exception e) {
            this.createError(element, e.getMessage());
        }
    }

    protected void checkURL() {
        FormElement element = this.fields.get("url");

        OutputStream stream = null;
        try {
            URL url = new URL((String) element.getValue());
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.connect();
            stream = connection.getOutputStream();
        } catch (Exception e) {
            this.createError(this.fields.get("url"), "Provided URL is invalid.");
        } finally {
            try {
                if (stream != null)
                    stream.close();
            } catch (IOException x) {
                // fail silently
            }
        }
    }

    protected void checkMeetingNotOnWeekend() {
        try {
            LocalDate date = (LocalDate) this.fields.get("date").getValue();
            DayOfWeek day = date.getDayOfWeek();
            if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY) {
                this.createError(this.fields.get("date"), "You cannot schedule an appointment on the weekend.");
            }
        } catch (Exception e) {
            this.createError(this.fields.get("date"), "You must select a date..");
        }
    }

    protected void checkMeetingInBusinessHours() {
        String startTime = (String) this.fields.get("startTime").getValue();
        String[] times = startTime.split(":");

        try {
            int hour = Integer.parseInt(times[0]);
            if (hour < 8 || hour >= 17) {
                this.createError(this.fields.get("startTime"),
                        "You cannot schedule a meeting to begin outside of 8am and 5pm.");
            }
            return;
        } catch (Exception e) {
            // pass
        }
        this.createError(this.fields.get("startTime"), "An unexpected error occurred.");
    }
}
