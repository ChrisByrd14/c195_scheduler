package scheduler.validators;

import java.util.Arrays;
import scheduler.FormElement;
import scheduler.ValidationError;

public class CustomerValidator extends Validator implements ValidatorInterface {

    protected String[] textFields = {"customerName", "streetAddress1", "city", "postalCode", "phoneNumber", "country"};

    /**
     * Verifies that each form element contains valid data, according to what we expect
     * @return true if data is valid
     */
    @Override
    public boolean validate() {
        if (this.fields == null || this.fields.isEmpty()) {
            this.error = new ValidationError(null, "No information provided.");
            return false;
        }

        Arrays.asList(this.textFields).forEach((key) -> {
            this.validateString((FormElement) this.fields.get(key));
        });

        if (this.error != null)
            return false;

        this.validatePostalCode();
        this.validatePhone();

        return this.error == null;
    }

    /**
     * Verify that the provided phone is valid (has 10 numeric digits)
     */
    private void validatePhone() {
        FormElement element = this.fields.get("phoneNumber");
        if ( ! this.checkDigitLength((String) element.getValue(), 10)) {
            this.createError(element, "Phone number is invalid. Must contain 10 digits.");
        }
    }

    /**
     * Verify that the provided postal code is valid (has 5 numeric digits)
     */
    private void validatePostalCode() {
        FormElement element = this.fields.get("postalCode");
        if ( ! this.checkDigitLength((String) element.getValue(), 5)) {
            this.createError(element, "Postal code is invalid. Must contain only 5 digits.");
        }
    }
}
