package scheduler.validators;

import java.util.Map;
import scheduler.FormElement;
import scheduler.ValidationError;

public interface ValidatorInterface {
    /**
     * Loads each validation service with associated form fields
     * @param fields ArraList consisting of all form fields
     * @return true if fields is not empty
     */
    public boolean load(Map<String, FormElement> fields);

    /**
     * Loops over each form element and verifies the data is valid
     * @return true if data is valid
     */
    public boolean validate();

    /**
     * Gets the first error that occurred in the data validation
     * @return Error object with the failed form element and error message
     */
    public ValidationError getError();

    /**
     * Clears the validation service's error instance variable
     */
    public void clearError();
}
