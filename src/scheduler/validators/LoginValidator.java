package scheduler.validators;

import java.util.Arrays;
import scheduler.FormElement;
import scheduler.ValidationError;

public class LoginValidator extends Validator implements ValidatorInterface {

    private String[] textFields = {"userName", "password"};

    /**
     * Validates that each form element's text value is not empty
     * @return true if data is valid
     */
    @Override
    public boolean validate() {
        if (this.fields == null || this.fields.isEmpty()) {
            this.error = new ValidationError(null, "No information provided.");
            return false;
        }

        Arrays.asList(this.textFields).forEach((key) -> {
            this.validateString((FormElement) this.fields.get(key));
        });
        return (this.error == null);
    }
}
