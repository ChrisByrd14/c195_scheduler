package scheduler.models;

import java.sql.ResultSet;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class EditCustomer extends Customer {
    private String tableActiveString;

    private String address;
    private String address2;
    private String postalCode;
    private String phoneNumber;

    private int cityID;
    private String city;

    private int countryID;
    private String country;

    public EditCustomer() {}

    public EditCustomer(int customerID, String customerName, int active, int addressID, String address1,
            String address2, String phone, String postalCode, int cityID, String city, int countryID, String country,
            String createdBy) {

        super(customerName, addressID, createdBy);

        this.setCustomerName(customerName);
        this.setActive(active);
        if (this.getActive() == 1) {
            this.setTableActiveString("✔");
        } else {
            this.setTableActiveString("-");
        }
        this.setID(customerID);
        this.setAddress(address1);
        this.setAddress2(address2);
        this.setPhoneNumber(phone);
        this.setPostalCode(postalCode);
        this.setCityID(cityID);
        this.setCity(city);
        this.setCountryID(countryID);
        this.setCountry(country);
    }

    /**
     * Retrieve all customers as EditCustomer objects
     * @return ObservableList of EditCustomer objects
     */
    public ObservableList<EditCustomer> getAllCustomers() {

        this.db.connect();

        String query = "SELECT * FROM `customer` c " +
                                                 "LEFT JOIN `address` a ON `c`.`addressId`=`a`.`addressId` " +
                                                 "LEFT JOIN `city` t ON `a`.`cityId`=`t`.`cityId` " +
	                               "LEFT JOIN `country` r ON `t`.`countryId`=`r`.`countryId`;";
        ObservableList<EditCustomer> customers = FXCollections.observableArrayList();
         try {
            Statement statement = this.db.getStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                customers.add(new EditCustomer(
                        result.getInt("c.customerId"),
                        result.getString("c.customerName"),
                        result.getInt("c.active"),
                        result.getInt("a.addressId"),
                        result.getString("a.address"),
                        result.getString("a.address2"),
                        result.getString("a.phone"),
                        result.getString("a.postalCode"),
                        result.getInt("t.cityId"),
                        result.getString("t.city"),
                        result.getInt("r.countryId"),
                        result.getString("r.country"),
                        result.getString("c.createdBy")
                ));
            }
            this.db.closeConnection();
            return customers;
        } catch (Exception e) {
            this.status = "GetAllCustomers Exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return null;
    }

    /**
     * Retrieve all active customers as EditCustomer object
     * @return ObservableList of EditCustomer objects
     */
    public ObservableList<EditCustomer> getAllActiveCustomers() {

        this.db.connect();

        String query = "SELECT * FROM `customer` c " +
                        "LEFT JOIN `address` a ON `c`.`addressId`=`a`.`addressId` " +
                        "LEFT JOIN `city` t ON `a`.`cityId`=`t`.`cityId` " +
                        "LEFT JOIN `country` r ON `t`.`countryId`=`r`.`countryId` " +
                        "WHERE `c`.`active`=1 ORDER BY `c`.`customerName`;";

        ObservableList<EditCustomer> customers = FXCollections.observableArrayList();

        try {
            Statement statement = this.db.getStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                customers.add(new EditCustomer(
                        result.getInt("c.customerId"),
                        result.getString("c.customerName"),
                        1,
                        result.getInt("a.addressId"),
                        result.getString("a.address"),
                        result.getString("a.address2"),
                        result.getString("a.phone"),
                        result.getString("a.postalCode"),
                        result.getInt("t.cityId"),
                        result.getString("t.city"),
                        result.getInt("r.countryId"),
                        result.getString("r.country"),
                        result.getString("c.createdBy")
                ));
            }
            this.db.closeConnection();
            return customers;
        } catch (Exception e) {
            this.status = "GetAllCustomers Exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return null;
    }

    /*
     * Getters and Setters below...
     */

    public String getTableActiveString() {
        return tableActiveString;
    }

    public void setTableActiveString(String tableActiveString) {
        this.tableActiveString = tableActiveString;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
