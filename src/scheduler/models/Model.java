/**
 * Abstract class for all model classes to reduce logic and property duplication
 */

package scheduler.models;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import scheduler.Database;

public abstract class Model {
    protected Database db;
    protected String status;

    protected String table = "";
    protected int ID = -1;
    protected Timestamp createdDate = null;
    protected String createdBy = null;
    protected Timestamp lastUpdate = null;
    protected String lastUpdateBy = null;

    public Model(String table, String createdBy) {
        this.setTable(table);
        this.setCreatedBy(createdBy);
        Calendar calendar = Calendar.getInstance();
        Timestamp currentTimestamp = new Timestamp(calendar.getTime().getTime());
        this.setCreatedDate(currentTimestamp);
        this.db = new Database();
    }

    /*
     * Getters and Setters below...
     */

    public String getTable() {
        return this.table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public int getID() {
        return this.ID;
    }

    public void setID(int id) {
        this.ID = id;
    }

    public Timestamp getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public String getStatus() {
        return this.status;
    }
}
