package scheduler.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Address extends Model {

    protected String address;
    protected String address2;
    protected int city;           // Foreign Key for City table
    protected String postalCode;
    protected String phone;

    public Address() {
        this("", "", -1, "", "", "");
    }

    public Address(String address, String address2, int city, String postalCode, String phone,  String createdBy) {
        super("address", createdBy);
        this.setAddress(address);
        this.setAddress2(address2);
        this.setCity(city);
        this.setPostalCode(postalCode);
        this.setPhone(phone);
    }

    public Address(int addressID, String address, String address2, int city, String postalCode, String phone,  String createdBy) {
        super("address", createdBy);
        this.setAddress(address);
        this.setAddress2(address2);
        this.setCity(city);
        this.setPostalCode(postalCode);
        this.setPhone(phone);
        this.setID(addressID);
    }

    /**
     * Insert address into the database
     * @param address address line 1
     * @param address2 address line 2
     * @param cityID id of the city record
     * @param postalCode five-digit postal code
     * @param phone ten-digit phone number
     * @param userID user ID to associate with this record
     * @return true on success
     */
    public boolean insert(String address, String address2, int cityID, String postalCode, String phone, int userID) {

        this.db.connect();

        String query = "INSERT INTO `address` " +
                       "(`address`, `address2`, `cityId`, `postalCode`, `phone`, " +
                       "`createdBy`, `createDate`, `lastUpdateBy`) VALUES " +
                       "(?, ?, ?, ?, ?, ?, NOW(), ?);";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, address);
            statement.setString(2, address2);
            statement.setInt(3, cityID);
            statement.setString(4, postalCode);
            statement.setString(5, phone);
            statement.setString(6, ""+userID);
            statement.setString(7, ""+userID);
            boolean result = statement.execute();
            this.db.closeConnection();
            return result;
        } catch (Exception e) {
            this.status = "Address insert exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return false;
    }

    /**
     * Get the address ID by address, city, and postal code
     * @param address address line 1
     * @param cityID city record ID
     * @param postalCode five-digit postal code
     * @return int ID of the address record, or -1 if failure
     */
    public int selectByAddressCityAndPostalCode(String address, int cityID, String postalCode) {

        if (address == null || address.trim().equals("")) {
            this.status = "Address is invalid.";
            return -1;
        }

        if (city < 1) {
            this.status = "City is invalid.";
            return -1;
        }

        if (postalCode == null || postalCode.trim().equals("")) {
            this.status = "Postal code is invalid.";
            return -1;
        }

        this.db.connect();

        String query = "SELECT `addressId` FROM `address` " +
                       "WHERE `address`=? AND `cityId`=? AND `postalCode`=? LIMIT 1";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, address);
            statement.setInt(2, cityID);
            statement.setString(3, postalCode);

            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int value = result.getInt("addressId");
                this.db.closeConnection();
                return value;
            }
        } catch (Exception e) {
            this.status = "Address select exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return -1;
    }

    /**
     * Retrieve record in address table
     * @param address address line 1
     * @param address2 address line 2
     * @param cityID id of the city record
     * @param postalCode five-digit postal code
     * @param phone ten-digit phone number
     * @return Address object that meets the criteria, null on failure
     */
    public Address findAddress(String address, String address2, int cityID, String postalCode, String phone) {

        this.db.connect();

        String query = "SELECT * FROM `address` WHERE " +
                "LOWER(`address`)=? AND LOWER(`address2`)=? AND `cityId`=? AND `postalCode`=? AND " +
                // All the REPLACES strip the phone number of any typical non-numeric digit
                "REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(`phone`, '-', ''), ' ', ''), '.', ''), ')', ''), '(', '')=? " +
                "LIMIT 1";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, address.toLowerCase());
            statement.setString(2, address2.toLowerCase());
            statement.setInt(3, cityID);
            statement.setString(4, postalCode);
            statement.setString(5, phone);

            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Address desiredAddress = new Address();
                desiredAddress.setID(result.getInt("addressId"));
                this.db.closeConnection();
                return desiredAddress;
            }
        } catch (Exception e) {
            this.status = "Address select exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return null;
    }

    /*
     * Getters and Setters below...
     */

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
