package scheduler.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Appointment extends Model {

    protected int customerID;
    protected String title;
    protected String description;
    protected String location;
    protected String contact;
    protected String url;
    protected Timestamp start;
    protected Timestamp end;
    protected String customerName;

    private boolean firstPass = true;

    protected Calendar startDate = Calendar.getInstance();
    protected Calendar endDate = Calendar.getInstance();

    public Appointment() {
        this(-1, "", "", "", "", "", null, null, "");
    }

    public Appointment(int customerID, String title, String description, String location, String contact, String url,
            Timestamp start, Timestamp end, String createdBy) {
        super("appointment", createdBy);
        this.setCustomerID(customerID);
        this.setTitle(title);
        this.setDescription(description);
        this.setLocation(location);
        this.setContact(contact);
        this.setUrl(url);
        this.setStart(start);
        this.setEnd(end);
    }

    /**
     * Constructor used by Appointment methods that return Appointment objects from database
     * @param customerID
     * @param title
     * @param description
     * @param location
     * @param contact
     * @param url
     * @param start
     * @param end
     * @param createDate
     * @param createdBy 
     */
    public Appointment(int customerID, String title, String description, String location, String contact, String url,
            Timestamp start, Timestamp end,Timestamp createDate, String createdBy) {
        super("appointment", createdBy);
        this.setCustomerID(customerID);
        this.setTitle(title);
        this.setDescription(description);
        this.setLocation(location);
        this.setContact(contact);
        this.setUrl(url);
        this.setStart(start);
        this.setEnd(end);
        this.setCreatedDate(createDate);
    }

    /**
     * Get all appointments coming up in the next 15 minutes for the provided user
     * @param user User object for logged-in user
     * @param date current date to check against
     * @param notInList list of invalid appointment IDs
     * @return ArrayList of all Appointments that meet the criteria
     */
    public ArrayList<Appointment> getUpcomingAppointments(User user, String date, ArrayList<Integer> notInList) {
        if (user == null) {
            this.status = "No user provided.";
            return null;
        }

        this.db.connect();

        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM `appointment` WHERE `start` BETWEEN ? AND ADDDATE(?, INTERVAL 15 MINUTE)");
        query.append(" AND `createdBy`=?");

        if ( ! notInList.isEmpty()) {
            query.append(" AND `appointmentId` NOT IN (");
            notInList.forEach(id -> {
                if ( ! this.firstPass)
                    query.append(", ");

                query.append(id);
                firstPass = false;
            });
            query.append(");");
        }

        ArrayList<Appointment> appointments = new ArrayList<Appointment>();
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query.toString());
            statement.setString(1, date);
            statement.setString(2, date);
            statement.setString(3, ""+user.getID());
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Appointment appointment = new Appointment(
                    result.getInt("customerId"),
                    result.getString("title"),
                    result.getString("description"),
                    result.getString("location"),
                    result.getString("contact"),
                    result.getString("url"),
                    result.getTimestamp("start"),
                    result.getTimestamp("end"),
                    result.getTimestamp("createDate"),
                    result.getString("createdBy")
                );
                appointment.setID(result.getInt("appointmentId"));
                appointments.add(appointment);
            }
            this.db.closeConnection();
            return appointments;
        } catch (Exception e) {
            this.status = "Upcoming Appointment Exception:\t" + e.getMessage();
            System.err.println(this.status);
        }
        return null;
    }

    /**
     * Get all appointments for the provided user
     * @param user User object for the logged-in user
     * @return ArrayList of all Appointment objects
     */
    public ArrayList<Appointment> getAllUserAppointments(User user) {
        if (user == null) {
            this.status = "No user provided.";
            return null;
        }

        this.db.connect();

        String query = "SELECT * FROM `appointment` a " +
                       "LEFT JOIN `customer` c ON `c`.`customerId`=`a`.`customerId` " +
                       "WHERE `a`.`createdBy`=? " +
                       "AND `a`.`start` <> '0000-00-00 00:00:00' AND `a`.`end` <> '0000-00-00 00:00:00' " +
                       "ORDER BY `a`.`start`";

        ArrayList<Appointment> appointments = new ArrayList<Appointment>();

        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, ""+user.getID());
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Appointment appointment = new Appointment(
                    result.getInt("customerId"),
                    result.getString("title"),
                    result.getString("description"),
                    result.getString("location"),
                    result.getString("contact"),
                    result.getString("url"),
                    result.getTimestamp("start"),
                    result.getTimestamp("end"),
                    result.getTimestamp("createDate"),
                    result.getString("createdBy")
                );
                appointment.setID(result.getInt("appointmentId"));
                appointments.add(appointment);
            }
            this.db.closeConnection();
            return appointments;
        } catch (Exception e) {
            e.printStackTrace();
            this.status = "User appointment query exception:\t" + e.getMessage();
            System.err.println(this.status);
        }
        return null;
    }

    /**
     * Get the count of all overlapping appointments
     * @param user User object of the logged-in user
     * @param startDateTime start time to check
     * @param endDateTime end time to check
     * @param appointmentID appointment ID to ignore (if editing appointment, as opposed to submitting new..)
     * @return count of appointments that overlap
     */
    public int getNumberOfOverlappingAppointments(User user, String startDateTime, String endDateTime,
            int appointmentID) {
        int countNumber = -1;

        if (user == null) {
            return countNumber;
        }

        this.db.connect();

        String query = "SELECT COUNT(*) AS `count` FROM appointment " +
                            "WHERE `createdBy`=? " +
                            "AND ((`start` BETWEEN ? AND ?) OR (`end` BETWEEN ? AND ? ))";
        if (appointmentID != -1)
            query = query + " AND `appointmentId`<>?";

        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, ""+user.getID());
            statement.setString(2, startDateTime);
            statement.setString(3, endDateTime);
            statement.setString(4, startDateTime);
            statement.setString(5, endDateTime);
            if (appointmentID != -1)
                statement.setInt(6, appointmentID);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                countNumber = result.getInt("count");
            }
            this.db.closeConnection();
            return countNumber;
        } catch (Exception e) {
            this.status = "Overlapping Appointment Query Exception:\t" + e.getMessage();
            System.err.println(this.status);
        }
        return countNumber;
    }

    /**
     * Insert new record into the database
     * @param customerID customer ID to associate with appointment
     * @param title appointment title
     * @param description appointment description
     * @param location appointment location
     * @param contact appointment contact
     * @param url appointment URL
     * @param start appointment start time
     * @param end appointment end time
     * @param userID User ID to associate with this record
     * @return true on success
     */
    public boolean createNewAppointment(int customerID, String title, String description, String location,
            String contact, String url, String start, String end, int userID) {

        this.db.connect();

        String query = "INSERT INTO `appointment` (`customerId`, `title`, `description`, `location`, `contact`, " +
                "`url`, `start`, `end`, `createDate`, `createdBy`, `lastUpdateBy`)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, ?);";
       try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setInt(1, customerID);
            statement.setString(2, title);
            statement.setString(3, description);
            statement.setString(4, location);
            statement.setString(5, contact);
            statement.setString(6, url);
            statement.setString(7, start);
            statement.setString(8, end);
            statement.setInt(9, userID);
            statement.setInt(10, userID);

            statement.execute();
            this.db.closeConnection();
            return true;
        } catch (Exception e) {
            this.status = "Insert appointment exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return false;
    }

    /**
     * Update a record in the database
     * @param appointmentID ID of record to update
     * @param customerID customer ID
     * @param title appointment title
     * @param description appointment description
     * @param location appointment location
     * @param contact appointment contact
     * @param url appointment URL
     * @param start appointment start time
     * @param end appointment end time
     * @param userID User ID to associate with this record
     * @return true on success
     */
    public boolean updateAppointment(int appointmentID, int customerID, String title, String description, String location,
            String contact, String url, String start, String end, int userID) {

        this.db.connect();

        String query = "UPDATE `appointment` SET " +
                "`customerId`=?, `title`=?, `description`=?, `location`=?, `contact`=?, " +
                "`url`=?, `start`=?, `end`=?, `lastUpdateBy`=? " +
                "WHERE `appointmentId`=?;";
       try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setInt(1, customerID);
            statement.setString(2, title);
            statement.setString(3, description);
            statement.setString(4, location);
            statement.setString(5, contact);
            statement.setString(6, url);
            statement.setString(7, start);
            statement.setString(8, end);
            statement.setInt(9, userID);
            statement.setInt(10, appointmentID);

            statement.execute();
            this.db.closeConnection();
            return true;
        } catch (Exception e) {
            this.status = "Update appointment exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return false;
    }

    /**
     * Retrieve all appointment types within a given date period
     * @param minDate minimum date to check against
     * @param maxDate maximum date to check against
     * @return ObservableList of AppointmentType objects (for TableView)
     */
    public ObservableList<AppointmentType> getAppointmentTypes(String minDate, String maxDate) {

        this.db.connect();

        ObservableList<AppointmentType> appointmentTypes = FXCollections.observableArrayList();

        String query = "SELECT `title`, COUNT(*) as `count` from `appointment` " +
                "WHERE `start` BETWEEN ? AND ? GROUP BY `title`;";

        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, minDate);
            statement.setString(2, maxDate);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                appointmentTypes.add(
                    new AppointmentType(
                        result.getString("title"),
                        result.getInt("count"))
                );
            }
            this.db.closeConnection();
            return appointmentTypes;
        } catch (Exception e) {
            this.status = "AppointmentTypes Query Exception:\t" + e.getMessage();
            System.err.println(this.status);
        }
        return null;
    }

    /**
     * Get all appointments, ordered by consultant
     * @return ObservableList of Appointment objects (for TableView)
     */
    public ObservableList<Appointment> getAllAppointmentsOrderedByUser() {

        this.db.connect();

        ObservableList<Appointment> appointments = FXCollections.observableArrayList();

        String query = "SELECT `title`, `location`, `start`, `end`, `userName` , `customerName` " +
                "FROM `appointment` a " +
                "LEFT JOIN `user` u ON `u`.`userId`=`a`.`createdBy` " +
                "LEFT JOIN `customer` c ON `c`.`customerId`=`a`.`customerId` " +
                "ORDER BY `a`.`createdBy`, `a`.`start`;";

        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Appointment appt = new Appointment(
                        -1,
                        result.getString("title"),
                        null,
                        result.getString("location"),
                        null,
                        null,
                        result.getTimestamp("start"),
                        result.getTimestamp("end"),
                        null,
                        result.getString("userName"));

                appt.setCustomerName(result.getString("customerName"));
                appt.setStartDate(this.convertUTCStringToDate(appt.getStart().toString()));

                appointments.add(appt);
            }
            this.db.closeConnection();
            return appointments;
        } catch (Exception e) {
            this.status = "AppointmentTypes Query Exception:\t" + e.getMessage();
            System.err.println(this.status);
        }
        return null;
    }

    /**
     * Delete a record from the database
     */
    public void removeFromDatabase() {
        this.db.connect();

        String query = "DELETE FROM `appointment` WHERE `appointmentId`=?;";
       try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setInt(1, this.getID());
            statement.execute();
            this.db.closeConnection();
        } catch (Exception e) {
            this.status = "Appointment Deletion Exception: " + e.getMessage();
            System.out.println(this.status);
        }
    }

    /*
     * Getters and Setters below...
     */

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public void setStartDate(Date startDate) {
        this.startDate.setTime(startDate);
    }

    public Calendar getStartDate() {
        return this.startDate;
    }

    public String getStartDateString() {
        return this.convertUTCStringToDate(this.getStart().toString()).toString();
    }

    public void setEndDate(Date endDate) {
        this.endDate.setTime(endDate);
    }

    public Calendar getEndDate() {
        return this.endDate;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    private Date convertUTCStringToDate(String utcDate) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(tz);

        try {
            return formatter.parse(utcDate);
        } catch (Exception e) {
            return null;
        }
    }
}
