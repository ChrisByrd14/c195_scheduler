package scheduler.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class City extends Model {

    protected String city;
    protected int countryID;

    public City() {
        this("", -1, "");
    }

    public City(String city, int countryID, String createdBy) {
        super("city", createdBy);
        this.setCity(city);
        this.setCountryID(countryID);
    }

    /**
     * Get all cities by name
     * @param cityName city name to look for
     * @return ArrayList of City objects
     */
    public ArrayList<City> getByName(String cityName) {
        ArrayList<City> cities = new ArrayList<City>();

        if (cityName == null || cityName.equals("")) {
            this.status = "City must not be empty.";
            return null;
        }

        this.db.connect();

        String query = "SELECT * FROM `city` WHERE LOWER(`city`)=?";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, cityName.toLowerCase());
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                City newCity = new City(
                    result.getString("city"),
                    result.getInt("countryId"),
                    "" + result.getInt("createdBy")
                );
                newCity.setID(result.getInt("cityId"));
                cities.add(newCity);
            }
            this.db.closeConnection();
            return cities;
        } catch (Exception e) {
            System.out.println("User appointment query exception:\t" + e);
        }
        return null;
    }

    /**
     * Insert new record into the database
     * @param userID User ID to associate with the record
     * @param cityName name of the city to be inserted
     * @param countryID ID of country this city is associate with
     * @return -1 on failure
     */
    public int insertNewCity(int userID, String cityName, int countryID) {
        if (cityName == null || cityName.equals("")) {
            this.status = "City must not be empty.";
            return -1;
        }

        this.db.connect();

        String query = "INSERT INTO `city` (`city`, `countryId`, `createdBy`, `lastUpdateBy`, `createDate`) " +
                       "VALUES (?, ?, ?, ?, NOW())";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, cityName);
            statement.setInt(2, countryID);
            statement.setInt(3, userID);
            statement.setInt(4, userID);
            int result = statement.executeUpdate();
            this.db.closeConnection();
            return result;
        } catch (Exception e) {
            System.out.println("City insertion query exception:\t" + e);
        }
        return -1;
    }

    /*
     * Getters and Setters below...
     */

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }
}
