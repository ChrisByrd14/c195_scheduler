package scheduler.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Customer extends Model {

    protected String customerName;
    protected int addressID;
    protected int active;

    protected Address address;

    public Customer() {
        this("", -1, "");
    }
    public Customer(String name, int addressID, String createdBy) {
        super("customer", createdBy);
    }

    /**
     * Insert record into the table
     * @param customerName name of the customer to create
     * @param addressID ID of associated address
     * @param active 1 for active, 0 for inactive
     * @param userID User record to associate with this record
     * @return true on success
     */
    public boolean insert(String customerName, int addressID, int active, int userID) {

        this.db.connect();

        String query = "INSERT INTO `customer` " +
                                "(`customerName`, `addressId`, `active`, `createDate`, `createdBy`, `lastUpdateBy`) " +
                                "VALUES (?, ?, ?, NOW(), ?, ?)";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, customerName);
            statement.setInt(2, addressID);
            statement.setInt(3, active);
            statement.setInt(4, userID);
            statement.setInt(5, userID);

            statement.execute();
            this.db.closeConnection();
            return true;
        } catch (Exception e) {
            this.status = "Insert customer exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return false;
    }

    /**
     * Update database record
     * @param customerID customer ID to update
     * @param customerName customer name
     * @param addressID customer address ID
     * @param active 1 for active, 0 for inactive
     * @param userID User record to associate with this record
     * @return true on success
     */
    public boolean update(int customerID, String customerName, int addressID, int active, int userID) {

        this.db.connect();

        String query = "UPDATE `customer` " +
                                "SET `customerName`=?, `addressId`=?, `active`=?, `lastUpdateBy`=? " +
                                "WHERE `customerId`=?";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, customerName);
            statement.setInt(2, addressID);
            statement.setInt(3, active);
            statement.setInt(4, userID);
            statement.setInt(5, customerID);

            statement.execute();
            this.db.closeConnection();
            return true;
        } catch (Exception e) {
            this.status = "Update Customer Exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return false;
    }

    /**
     * Retrieve by ID
     * @param customerID customer ID
     * @return Customer object
     */
    public Customer getByID(int customerID) {
        String query = "SELECT * FROM `customer` WHERE `customerId`=? LIMIT 1;";

        this.db.connect();

        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setInt(1, customerID);

            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer();
                customer.setActive(result.getInt("active"));
                customer.setAddressID(result.getInt("addressId"));
                customer.setCreatedBy(result.getString("createdBy"));
                customer.setCreatedDate(result.getTimestamp("createDate"));
                customer.setCustomerName(result.getString("customerName"));
                customer.setID(result.getInt("customerId"));
                customer.setLastUpdate(result.getTimestamp("lastUpdate"));
                customer.setLastUpdateBy(result.getString("lastUpdateBy"));
                this.db.closeConnection();
                return customer;
            }
            throw new Exception("Unexpected error occurred.");
        } catch (Exception e) {
            System.err.println("Customer By ID Exception:\t" + e.getMessage());
        }
        return null;
    }

    /*
     * Getters and Setters below...
     */

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}
