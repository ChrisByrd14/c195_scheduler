/**
 * Wrapper class for Login Log Report TableView
 */
package scheduler.models;

public class LoginLog {
    private String recordDate;
    private String recordText;

    public LoginLog(String[] record) {
        this.recordDate = record[0];
        this.recordText = record[1];
    }

    /*
     * Getters and Setters below...
     */

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    public String getRecordText() {
        return recordText;
    }

    public void setRecordText(String recordText) {
        this.recordText = recordText;
    }
}
