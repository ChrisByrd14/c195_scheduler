package scheduler.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Country extends Model {

    protected String country;

    public Country() {
        this(null, null);
    }

    public Country(String country, String createdBy) {
        super("country", createdBy);
        this.setCountry(country);
    }

    /**
     * Get all country names
     * @return ObservableList of all country names (for ChoiceBox)
     */
    public ObservableList<String> getCountryNames() {

        this.db.connect();

        ObservableList<String> countries = FXCollections.observableArrayList();
        String query = "SELECT `countryId`, `country` FROM `country`;";

        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                countries.add(result.getString("country"));
            }
            this.db.closeConnection();
            return countries;
        } catch (Exception e) {
            this.status = "Country name query exception:\t" + e.getMessage();
            System.out.println(this.status);
        }
        return null;
    }

    /**
     * Retrieve the ID of the associated record
     * @param country country name to look for
     * @return ID of the associate record
     */
    public int getCountryID(String country) {
        if (country == null || country.trim().equals("")) {
            this.status = "Please provide a country.";
            return -1;
        }

        this.db.connect();

        String query = "SELECT `countryId` FROM `country` WHERE LOWER(`country`)=?";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, country);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                int value = result.getInt("countryId");
                this.db.closeConnection();
                return value;
            }
        } catch (Exception e) {
            this.status = "Country ID Exception: " + e.getMessage();
            System.out.println(this.status);
        }
        return -1;
    }

    /*
     * Getters and setters below...
     */

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
