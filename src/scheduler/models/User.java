package scheduler.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import scheduler.Password;

public class User extends Model {

    protected String userName;
    protected String password;
    protected boolean active;

    public User() {
        this("", "", 0, "admin");
    }

    public User(String username, String password, int active, String createdBy) {
        super("user", createdBy);
        this.setUserName(username);
        this.setPassword(password);
        this.active = (active == 1);
    }

    /**
     * Attempt to retrieve user for log-in purposes
     * @param username username to look for
     * @param password hashed password to look for
     * @return User object on success
     */
    public User tryLogin(String username, String password) {
        String hashedPassword = Password.hashPassword(password, username);

        if (hashedPassword == null) {
            this.status = "Unable to hash the password with SHA-256";
            return null;
        }

        this.db.connect();

        String query = "SELECT * FROM `user` " +
                       "WHERE `userName`=? AND `password`=? LIMIT 1";
        try {
            PreparedStatement statement = this.db.getPreparedStatement(query);
            statement.setString(1, username);
            statement.setString(2, hashedPassword);
            ResultSet result = statement.executeQuery();

            User currentUser = null;
            while (result.next()) {
                currentUser = new User(result.getString("userName"),
                        result.getString("password"), result.getInt("active"), "");
                currentUser.setID(result.getInt("userId"));
            }
            this.db.closeConnection();
            return currentUser;
        } catch (Exception e) {
            this.status = "Login query exception:\t" + e.getMessage();
            System.out.println(this.status);
        }
        return null;
    }

    /*
     * Getters and Setters below...
     */

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
