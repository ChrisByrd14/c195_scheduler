package scheduler.controllers;

import java.io.File;
import java.util.ArrayList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import scheduler.AppointmentCalendar;
import scheduler.factories.AppointmentFactory;
import scheduler.models.Appointment;
import scheduler.models.User;
import scheduler.services.NotificationService;

public class MainScreenController extends BaseController {
    private User user;
    private NotificationService notificationService;
    private AppointmentCalendar calendar;
    private ArrayList<Appointment> appointments;
    private MediaPlayer notificationPlayer;

    @FXML private VBox calendarView;

    @FXML private Label notificationLabel;
    @FXML private Pane notificationPane;
    @FXML private Label closeNotificationButton;

    // Buttons
    @FXML private Button newCustomer;
    @FXML private Button newAppointment;
    @FXML private Button prevCalButton;
    @FXML private Button nextCalButton;
    @FXML private Button reloadAppointmentsButton;

    // Menu Items
    @FXML private MenuItem newCustomerMenuItem;
    @FXML private MenuItem editCustomerMenuItem;
    @FXML private MenuItem newAppointmentMenu;
    @FXML private CheckMenuItem notificationSoundEnable;
    @FXML private MenuItem appointmentTypesByMonthReport;
    @FXML private MenuItem consultantSchedulesReport;
    @FXML private MenuItem loginhistory;
    @FXML private MenuItem exitProgram;

    private final ToggleGroup calendarToggleGroup = new ToggleGroup();
    @FXML private ToggleButton monthToggleButton;
    @FXML private ToggleButton weekToggleButton;

    public MainScreenController() {}

    /**
     * Set up the controller to display information to the user
     * @param user  User object of the authenticated use
     */
    public void initData(User user) {
        this.user = user;

        this.monthToggleButton.setSelected(true);
        this.monthToggleButton.setToggleGroup(this.calendarToggleGroup);
        this.weekToggleButton.setToggleGroup(this.calendarToggleGroup);

        this.calendar = new AppointmentCalendar(this.user, this, this.calendarView);
        this.calendar.loadMonthView();

        NotificationService notificationService = new NotificationService(new AppointmentFactory(), this.user);
        notificationService.setPeriod(Duration.seconds(60));
        notificationService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                NotificationService service = (NotificationService) event.getSource();
                ArrayList<Appointment> appointments = service.getAppointments();

                if (appointments != null && !appointments.isEmpty()) {
                    System.out.println(appointments.get(0).getID());
                    service.addAppointmentToNotified(appointments.get(0));
                    displayNotification(appointments.get(0));
                }
            }
        });
        notificationService.start();

        // set chime to play when notifying user of upcoming appointments
        File chimeFile = new File("src/scheduler/sounds/chime.mp3");
        if (chimeFile.exists()) {
            Media chime = new Media(chimeFile.toURI().toString());
            this.notificationPlayer = new MediaPlayer(chime);
            this.notificationPlayer.setVolume(0.25);
        }

        this.notificationLabel.visibleProperty().bind(this.notificationPane.visibleProperty());
    }

    /**
     * Display the upcoming appointment on the view.
     * @param appointment Object representing the upcoming appointment
     */
    public void displayNotification(Appointment appointment) {
        try {
            StringBuilder message = new StringBuilder();
            message.append("You have an upcoming appointment with ");
            message.append(appointment.getContact());
            message.append(" at ");
            message.append(appointment.getLocation());
            message.append("\n\n");
            message.append("Details:\n");
            message.append(appointment.getDescription());

            this.notificationPane.setVisible(true);
            this.notificationLabel.setText(message.toString());
            if (this.notificationSoundEnable.isSelected()) {
                this.notificationPlayer.play();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Change which calendar object is being displayed to month
     */
    @FXML private void setCalendarToMonthView() {
        this.calendar.loadMonthView();
    }

    /**
     * change which calendar object is being displayed to week
     */
    @FXML private void setCalendarToWeekView() {
        this.calendar.loadWeekView();
    }

    /**
     * Launch the NewCustomer view
     */
    @FXML private void launchNewCustomer() {
        NewCustomerController controller = (NewCustomerController)
                this.launchView("NewCustomer", "Add New Customer");
        controller.initData(this.user);
    }

    /**
     * Close the appointment notification pane
     */
    @FXML private void closeNotification() {
        this.notificationLabel.setText("");
        this.notificationPane.setVisible(false);
    }

    /**
     * Launch the EditCustomer view
     */
    @FXML private void launchEditCustomer() {
        EditCustomerSelectionController controller = (EditCustomerSelectionController)
                this.launchView("EditCustomerSelection", "Select a customer to edit.");
        controller.initData(this.user);
    }

    /**
     * Launch the NewAppointment view
     */
    @FXML private void launchNewAppointment() {
        NewAppointmentController controller = (NewAppointmentController)
                this.launchView("NewAppointment", "Schedule a new appointment.");
        controller.initData(this.user, this.calendar);
    }

    /**
     * Change which dates are displayed on the calendar to the previous calendar unit (month/week)
     */
    @FXML private void getCalendarForPreviousCalendarUnit() {
        this.calendar.getPrevious();
    }

    /**
     * Change which dates are displayed on the calendar to the next calendar unit (month/week)
     */
    @FXML private void getCalendarForNextCalendarUnit() {
        this.calendar.getNext();
    }

    /**
     * Ask customer for confirmation to close the program
     */
    @FXML private void launchCloseProgram() {
        this.presentClosePrompt();
    }

    /**
     * Launch the AppointmentDetail view
     * @param appointment Appointment to populated the Detail view with
     */
    public void viewAppointmentDetails(Appointment appointment) {
        AppointmentDetailController controller = (AppointmentDetailController)
                this.launchView("AppointmentDetail", "Appointment Details");
        controller.initData(appointment, this.user, this.calendar);
    }

    /**
     * Launch Appointment Type by Month Report view
     */
    @FXML private void launchAppointmentTypeByMonthReport() {
        AppointmentTypeByMonthController controller = (AppointmentTypeByMonthController)
                this.launchView("AppointmentTypeByMonth", "Appointment Type By Month");
        controller.initData();
    }

    /**
     * Launch Consultant Schedule Report view
     */
    @FXML private void launchConsultantScheduleReport() {
        ConsultantScheduleReportController controller = (ConsultantScheduleReportController)
                this.launchView("ConsultantScheduleReport", "Consultant Schedules");
        controller.initData();
    }

    /**
     * Launch Login History report
     */
    @FXML private void launchLoginHistoryReport() {
        LoginHistoryReportController controller = (LoginHistoryReportController)
                this.launchView("LoginHistoryReport", "Login History Report");
        controller.initData();
    }
}
