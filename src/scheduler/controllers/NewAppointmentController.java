package scheduler.controllers;

import javafx.fxml.FXML;
import scheduler.AppointmentCalendar;
import scheduler.FormElement;
import scheduler.ValidationError;
import scheduler.models.User;

public class NewAppointmentController extends AppointmentController {

    public NewAppointmentController() {
        super();
    }

    /**
     * Initialize the New Appointment view
     * @param user logged in user
     * @param appointmentCalendar calendar to update after appointment submission
     */
    public void initData(User user, AppointmentCalendar appointmentCalendar) {
        this.user = user;
        this.appointmentCalendar = appointmentCalendar;
        this.customers = this.customerFactory.getAllActiveCustomers();
        this.customerTable.setItems(this.customers);
    }

    /**
     * Validate the appointment and insert into database
     */
    @FXML private void submitAppointment() {
        this.lockForm();
        this.setFields();

        this.validator.load(this.fields);

        if (!this.validator.validate()) {
            this.setError(this.validator.getError());
            return;
        }

        int count = this.factory.checkOverlappingAppointments(this.user, this.fields, -1);

        if (count > 0) {
            this.setError(new ValidationError(
                    new FormElement(null, this.startTime, null),
                    "There is already an appointment scheduled at this time."));
            return;
        }

        if (this.factory.createNewAppointment(this.fields, this.user)) {
            this.appointmentCalendar.reload();
            this.promptNotification("Appointment created successfully.");
            this.closeWindow();
            return;
        }
        this.promptError("An unknown error occurred.");
    }
}
