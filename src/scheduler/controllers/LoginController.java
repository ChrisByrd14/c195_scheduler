package scheduler.controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import scheduler.FormElement;
import scheduler.LoginAuditor;
import scheduler.factories.UserFactory;
import scheduler.models.User;
import scheduler.validators.LoginValidator;

public class LoginController extends BaseController implements Initializable {

    private final LoginValidator validator = new LoginValidator();
    private final LoginAuditor auditor = new LoginAuditor();
    private final UserFactory factory = new UserFactory();
    private final HashMap<String, FormElement> fields = new HashMap<>();

    private String loginError = "";
    private String emptyError = "";
    private boolean isEnglish;

    @FXML private Label usernameLabel;
    @FXML private Label passwordLabel;
    @FXML private Label loginLabel;
    @FXML private TextField usernameField;
    @FXML private PasswordField passwordField;
    @FXML private Hyperlink languageLink;
    @FXML private Button submitButton;

    /**
     * Initialize the Login View.
     * @param url [ unused ]
     * @param rb [unused ]
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
         * Uncomment the line below to change the
         * default language from to Spanish
         */
//         Locale.setDefault(new Locale("es-MX", "MX"));

        String currentLanguage = Locale.getDefault().getDisplayLanguage();
        this.isEnglish = currentLanguage.equalsIgnoreCase("es-mx");
        this.changeLanguage();
    }

    /**
     * Set the form fields to this.fields HashMap
     */
    private void setFields() {
        this.fields.clear();
        this.fields.put("userName", new FormElement("User Name", this.usernameField, this.usernameField.getText()));
        this.fields.put("password", new FormElement("Password", this.passwordField, this.passwordField.getText()));
    }

    /**
     * Validate form fields then check DB for a user that has the matching userName and hashed password
     */
    @FXML private void tryLogin() {
        this.lockForm();
        this.validator.clearError();

        this.validator.load(this.fields);
        if ( ! this.validator.validate()) {
            this.loginFailure(this.emptyError);
            return;
        }

        User user = this.factory.loadFromDB(this.fields);
        if (user == null) {
            this.loginFailure(this.loginError);
            return;
        }

        this.auditor.logSuccessfulLogin((String) this.fields.get("userName").getValue());
        MainScreenController controller = (MainScreenController)
                this.launchView("MainScreen", "Scheduler Application", "styles");
        controller.initData(user);
        this.closeWindow();
    }

    /**
     * Lock the form from being sent again
     */
    private void lockForm() {
        this.submitButton.setDisable(true);
        this.setFields();
    }

    /**
     * Unlock the form, clear the text fields, and present the error message
     * @param message text to display to the user
     */
    private void loginFailure(String message) {
        this.submitButton.setDisable(false);
        this.fields.forEach((key, field) -> {
            ((TextField) field.getElement()).clear();
        });
       this.auditor.logFailedLogin((String) this.fields.get("userName").getValue());
       this.promptError(message);
    }

    /**
     * Ask for confirmation to close the program
     */
    @FXML private void closeProgram() {
        this.presentClosePrompt();
    }

    /**
     * Toggle the language between English and Spanish
     */
    @FXML private void changeLanguage() {
        this.isEnglish = ! this.isEnglish;

        if ( ! this.isEnglish) {
            this.loginError = "Credenciales incorrectas.";
            this.emptyError = "Los campos no pueden estar vacíos.";
            this.loginLabel.setText("Iniciar sesión");
            this.submitButton.setText("Iniciar sesión");
            this.cancelButton.setText("Cancelar");
            this.usernameField.setPromptText("Usuario");
            this.passwordField.setPromptText("Contraseña");
            this.languageLink.setText("English");
            this.usernameLabel.setText("Usuario");
            this.passwordLabel.setText("Contraseña");
            return;
        }

        this.loginError = "Incorrect credentials.";
        this.emptyError = "Fields can't be empty.";
        this.loginLabel.setText("Login");
        this.submitButton.setText("Login");
        this.cancelButton.setText("Cancel");
        this.usernameField.setPromptText("Username");
        this.passwordField.setPromptText("Password");
        this.languageLink.setText("Español");
        this.usernameLabel.setText("Username");
        this.passwordLabel.setText("Password");
    }
}
