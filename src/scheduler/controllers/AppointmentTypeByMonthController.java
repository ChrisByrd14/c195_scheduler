package scheduler.controllers;

import java.util.Calendar;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import scheduler.factories.AppointmentFactory;
import scheduler.models.AppointmentType;

public class AppointmentTypeByMonthController extends ReportController {

    private Calendar calendar = Calendar.getInstance();
    private ObservableList<AppointmentType> records;
    private AppointmentFactory factory;

    @FXML private Button nextButton;
    @FXML private Button previousButton;

    @FXML private Label monthLabel;

    @FXML private TableView<AppointmentType> appointmentTypeTable = new TableView<AppointmentType>();
    @FXML private TableColumn<AppointmentType, String> appointmentTypeColumn = new TableColumn<AppointmentType, String>();
    @FXML private TableColumn<AppointmentType, Integer> appointmentTypeCountColumn = new TableColumn<AppointmentType, Integer>();

    /**
     * Initialize the data for the report
     */
    public void initData() {
        this.factory = new AppointmentFactory();
        this.loadData();
    }

    /**
     * Display the next month
     */
    @FXML private void getNext() {
        this.calendar.add(Calendar.MONTH, 1);
        this.loadData();
    }

    /**
     * Display the previous month
     */
    @FXML private void getPrevious() {
        this.calendar.add(Calendar.MONTH, -1);
        this.loadData();
    }

    /**
     * Load AppointmentType information to the TableView
     */
    private void loadData() {
        StringBuilder minDate = new StringBuilder();
        minDate.append(this.calendar.get(Calendar.YEAR));
        minDate.append("-");
        minDate.append(this.calendar.get(Calendar.MONTH) + 1);
        minDate.append("-1");

        StringBuilder maxDate = new StringBuilder();
        maxDate.append(this.calendar.get(Calendar.YEAR));
        maxDate.append("-");
        maxDate.append(this.calendar.get(Calendar.MONTH) + 1);
        maxDate.append("-");
        maxDate.append(this.calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        this.records = this.factory.getAppointmentTypes(minDate.toString(), maxDate.toString());
        this.appointmentTypeTable.getItems().clear();
        this.appointmentTypeTable.setItems(this.records);

        this.monthLabel.setText(minDate.toString() + " - " + maxDate.toString());

        this.generateFileText();
    }

    /**
     * Generate the String that will be used when writing to a file
     */
    private void generateFileText() {
        // clear file text
        this.fileText.setLength(0);

        // set file header
        this.fileText.append("\"");
        this.fileText.append(this.monthLabel.getText());
        this.fileText.append("\"\n");
        this.fileText.append("\"Appointment Type\",\"Number\"\n");

        this.records.forEach(record -> {
            this.fileText.append("\"");
            this.fileText.append(record.getType());
            this.fileText.append("\",\"");
            this.fileText.append(record.getNumber());
            this.fileText.append("\"\n");
        });
    }
}
