/**
 * Base for all controllers houses several helper function for notifying users of errors, getting confirmation, etc... 
 */
package scheduler.controllers;

import java.util.Optional;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class BaseController {

    @FXML protected Button cancelButton;

    /**
     * Launch view without additional style sheet
     * @param viewFile name of the FXML file to load (without .fxml)
     * @param title title to display on the newly initialized view
     * @return controller from the newly launched view
     */
    protected BaseController launchView(String viewFile, String title) {
        return this.launchView(viewFile, title, null);
    }

    /**
     * Launch view with additional style sheet, or (if styles == null), just launch the view)
     * @param viewFile name of the FXML file to load (without .fxml)
     * @param title title to display on the newly initialized view
     * @param styles name of the CSS file to launch and apply to the newly launched view
     * @return controller from the newly launched view
     */
    protected BaseController launchView(String viewFile, String title,
            String styles) {
        String path = "/scheduler/views/" + viewFile + ".fxml";
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            Parent loaderRoot = (Parent) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL); // prevent user from ignoring this window
            stage.setTitle(title);
            Scene scene = new Scene(loaderRoot);
            if (styles != null)
                this.loadStyleSheet(scene, styles);
            this.loadStyleSheet(scene, "styles");
            stage.setScene(scene);
            stage.show();
            BaseController controller = loader.<BaseController>getController();
            return controller;
        } catch (Exception e) {
            System.out.println("baseController.launchView Exception: " + e);
        }
        return null;
    }

    /**
     * Load the provided stylesheet to the newly opened Scene
     * @param scene Scene of the newly luanched view
     * @param stylesheet name of CSS file (without .css)
     * @return true on success
     */
    protected boolean loadStyleSheet(Scene scene, String stylesheet) {
        String css = "/scheduler/styles/" + stylesheet + ".css";
        boolean result;
        result = scene.getStylesheets().add(getClass().getResource(css).toExternalForm());
        return result;
    }

    /**
     * Ask the user for confirmation to close the window, if user confirms then close the window
     */
    protected void presentClosePrompt() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Close the current window?", ButtonType.OK, ButtonType.CANCEL);

        // Requirement G
        alert.showAndWait().ifPresent((selection) -> {
            if (selection == ButtonType.OK) {
                this.closeWindow();
            }
        });
    }

    /**
     * Ask the user for confirmation to complete a task. Return a boolean value for the user's response
     * @return true if user clicks OK
     */
    protected boolean promptConfirmation(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message, ButtonType.OK, ButtonType.CANCEL);
        // Requirement G
        Optional<ButtonType> result = alert.showAndWait();
        return (result != null && result.get() == ButtonType.OK);
    }

    /**
     * Display a non-error notification message to the user
     * @param message text to display to the user
     */
    protected void promptNotification(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION , message, ButtonType.OK);
        alert.showAndWait();
    }

    /**
     * Display an error notification to the user
     * @param message error message to display
     */
    protected void promptError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR , message, ButtonType.OK);
        alert.showAndWait();
    }

    /**
     * Show error to the user
     * @param message error message to display
     */
    protected void showError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR , message, ButtonType.OK);
        alert.showAndWait();
    }

    /**
     * Close the window calling this function
     */
    protected void closeWindow() {
        Stage stage = (Stage) this.cancelButton.getScene().getWindow();
        stage.close();
    }
}
