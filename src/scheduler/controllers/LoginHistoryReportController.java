package scheduler.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import scheduler.LoginAuditor;
import scheduler.models.LoginLog;

public class LoginHistoryReportController extends ReportController {

    private LoginAuditor auditor;
    private ObservableList<LoginLog> records;

    @FXML private TableView<LoginLog> loginLogTable = new TableView<LoginLog>();
    @FXML private TableColumn<LoginLog, String> loginDateTime = new TableColumn<LoginLog, String>();
    @FXML private TableColumn<LoginLog, String> loginText = new TableColumn<LoginLog, String>();

    /**
     * Initialize the LoginHistoryReport view
     */
    public void initData() {
        this.auditor = new LoginAuditor();
        this.records = this.auditor.readLog();
        this.loginLogTable.setItems(this.records);

        this.fileText.append("\"Date & Time\", \"Message\"\n");

        this.records.forEach(record -> {
            this.fileText.append("\"");
            this.fileText.append(record.getRecordDate());
            this.fileText.append("\", \"");
            this.fileText.append(record.getRecordText().replace("\"", "\\\""));
            this.fileText.append("\"\n");
        });
    }
}
