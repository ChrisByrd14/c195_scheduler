package scheduler.controllers;

import javafx.fxml.FXML;
import scheduler.ValidationError;
import scheduler.models.Address;
import scheduler.models.City;
import scheduler.models.EditCustomer;
import scheduler.models.User;

public class EditCustomerController extends CustomerController {

    private EditCustomer customer;

    /**
     * Call super class constructor to begin controller initializatoin
     */
    public EditCustomerController() {
        super();
    }

    /**
     * IFinish initializing the controller
     * @param user object representing the logged-in user
     * @param customer object representing the customer to be edited
     */
    public void initData(User user, EditCustomer customer) {
        this.setUpCustomerController(user);
        this.customer = customer;

        this.customerName.setText(this.customer.getCustomerName());
        this.streetAddress1.setText(this.customer.getAddress());
        this.streetAddress2.setText(this.customer.getAddress2());
        this.phoneNumber.setText(this.customer.getPhoneNumber());
        this.city.setText(this.customer.getCity());
        this.postalCode.setText(this.customer.getPostalCode());

        if (this.customer.getActive() == 1) {
            this.activeRadioButton.setSelected(true);
        } else {
            this.inactiveRadioButton.setSelected(true);
        }

        // select customer's country from choice box
        for (Object object : this.country.getItems()) {
            String currentCountry = (String) object;
            if (customer.getCountry().equals(currentCountry)) {
                this.country.getSelectionModel().select(currentCountry);
                break;
            }
        }
    }

    /**
     * Validate form fields and update them customer record in the database
     */
    @FXML private void submitNewCustomerDetails() {
        this.lockForm();

        this.validator.load(this.fields);
        if ( ! this.validator.validate()) {
            this.setError(this.validator.getError());
            return;
        }

        int countryID = this.countryFactory.getIDByName((String) this.fields.get("country").getValue());
        if (countryID < 1) {
            this.setError(new ValidationError(null, "Invalid country selected."));
            return;
        }

        City city = this.cityFactory.getOrAdd(this.fields.get("city"), countryID, this.user.getID());
        if (city == null || city.getID() < 1) {
            this.setError(new ValidationError(null, "Unable to retrieve city information."));
            return;
        }

        Address address = this.addressFactory.getOrAdd(
                this.fields.get("streetAddress1"),
                this.fields.get("streetAddress2"),
                city.getID(),
                this.fields.get("postalCode"),
                this.fields.get("phoneNumber"),
                this.user.getID());
        if (address == null || address.getID() < 0) {
            this.setError(new ValidationError(null, "Unable to retrieve address information."));
            return;
        }

        boolean updateSuccess = this.customerFactory.updateCustomer(
                this.customer.getID(),
                this.fields.get("customerName"),
                this.fields.get("active"),
                address.getID(),
                this.user.getID()
        );

        if (updateSuccess) {
            this.promptNotification("Customer updated successfully.");
            this.closeWindow();
            return;
        }

        this.setError(new ValidationError(null, "Unable to create new customer record.\nCheck console logs."));
    }
}
