package scheduler.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import javafx.fxml.FXML;
import static javafx.util.Duration.millis;
import scheduler.AppointmentCalendar;
import scheduler.FormElement;
import scheduler.ValidationError;
import scheduler.models.Appointment;
import scheduler.models.User;

public class EditAppointmentController extends AppointmentController {

    private TimeZone zone;
    private Appointment appointment;
    private AppointmentCalendar appointmentCalendar;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

    public EditAppointmentController() {
        super();
    }

    /**
     * Initialize the controller. Load the form with appointment information
     * @param user logged in user
     * @param appointment Appointment being edited currently
     * @param appointmentCalendar Calendar object to update on submission
     */
    public void initData(User user, Appointment appointment, AppointmentCalendar appointmentCalendar) {
        this.zone = TimeZone.getDefault();
        this.user = user;
        this.appointment = appointment;
        this.appointmentCalendar = appointmentCalendar;
        this.customers = this.customerFactory.getAllActiveCustomers();
        this.customerTable.setItems(this.customers);
        this.customerTable.getSelectionModel().select(appointment.getCustomerID() - 1);

        this.titleField.setText(appointment.getTitle());
        this.customerContact.setText(appointment.getContact());
        this.description.setText(appointment.getDescription());
        this.locationField.setText(appointment.getLocation());

        this.url.setText(appointment.getUrl());

        LocalDateTime start = this.getLocalDateTime(appointment.getStart());
        String startTimeString = start.format(this.formatter);

        this.meetingDate.setValue(start.toLocalDate());
        this.startTime.setText(startTimeString);

        LocalDateTime end = this.getLocalDateTime(appointment.getEnd());
        String endTimeString = end.format(this.formatter);


        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        long durationTime = 0;
        try {
            Date startDate = format.parse(startTimeString);
            Date endDate = format.parse(endTimeString);
            durationTime = endDate.getTime() - startDate.getTime();
        } catch (Exception e) {
            // pass
        }

        TimeUnit hour = TimeUnit.HOURS;
        TimeUnit millis = TimeUnit.MILLISECONDS;
        long minutes = millis.toMinutes(durationTime) - hour.toMinutes(millis.toHours(durationTime));
        String durationString = String.format("%02d:%02d", millis.toHours(durationTime), minutes);
        this.duration.setText(durationString);
    }

    /**
     * Submit the form data for validation and updating in the database
     */
    @FXML private void submitAppointment() {
        this.lockForm();
        this.setFields();

        this.validator.load(this.fields);

        if (!this.validator.validate()) {
            this.setError(this.validator.getError());
            return;
        }

        int count = this.factory.checkOverlappingAppointments(this.user, this.fields, this.appointment.getID());
        if (count > 0) {
            this.setError(new ValidationError(
                    new FormElement(null, this.startTime, null),
                    "There is already an appointment scheduled at this time."));
            return;
        }

        this.fields.put("appointmentID", new FormElement(null, null, ""+this.appointment.getID()));
        this.fields.put("user", new FormElement("User", null, this.user.getID()));
        if (this.factory.updateAppointment(this.fields, this.user)) {
            this.appointmentCalendar.reload();
            this.promptNotification("Appointment updated successfully.");
            this.closeWindow();
            return;
        }
        this.promptError("An unknown error occurred.");
    }

    /**
     * Helper function to convert UTC timestamp to a LocalDateTime object
     * @param stamp UTC timestamp from the database
     * @return LocalDateTime object correlating to the provided timestamp
     */
    private LocalDateTime getLocalDateTime(Timestamp stamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(stamp.getTime());
        calendar.add(Calendar.MILLISECOND, this.zone.getOffset(calendar.getTimeInMillis()));
        return LocalDateTime.ofInstant(calendar.toInstant(), ZoneId.systemDefault());
    }
}
