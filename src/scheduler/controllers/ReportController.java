/**
 * Super class for Report controllers used to share functionality
 */
package scheduler.controllers;

import java.io.File;
import java.io.FileWriter;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;

public class ReportController extends BaseController {
    protected StringBuilder fileText = new StringBuilder();

    @FXML protected Button saveButton;

    /**
     * Ask for confirmation to close the window
     */
    @FXML protected void launchClosePrompt() {
        this.presentClosePrompt();
    }

    /**
     * Attempt to write the report file
     * @param file File object containing filename to write to
     */
    protected void writeFile(File file) {
        try (FileWriter writer = new FileWriter(file);) {
            writer.write(this.fileText.toString());
        } catch (Exception e) {
            System.err.println("File Save Exception:\t" + e.getMessage());
        }
    }

    /**
     * Get the desired file to save to and call this.writeFile
     */
    @FXML protected void saveDataToCSVFile() {
        FileChooser chooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        chooser.getExtensionFilters().add(extFilter);
        File saveFile = chooser.showSaveDialog(this.cancelButton.getScene().getWindow());

        if (saveFile == null)
            return;

        this.writeFile(saveFile);
    }
}
