package scheduler.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import scheduler.ValidationError;
import scheduler.models.Address;
import scheduler.models.City;
import scheduler.models.User;

public class NewCustomerController extends CustomerController {

    /**
     * Initialize data and form elements on NewCustomer FXML form
     * @param user Object of the logged in user
     */
    public void initData(User user) {
        this.setUpCustomerController(user);

        // initial check for non-numerics in postal code.
        // final content/length checks will be performed by submitNewCustomer()
        this.postalCode.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                TextField source = (TextField) event.getSource();
                String digits = source.getText().trim().replaceAll("[^\\d]", "");
                source.setText(digits);
                source.positionCaret(digits.length());
            }
        });
    }

    /**
     * Validate the user-supplied information.
     * Call NewCustomerValidatorService and if valid, calls CustomerFactory, CityFactory, and AddressFactory
     * to create the data model objects that will insert the records into the database
     */
    @FXML private void submitNewCustomer() {
        this.lockForm();

        this.validator.load(this.fields);
        if ( ! this.validator.validate()) {
            this.setError(this.validator.getError());
            return;
        }

        int countryID = this.countryFactory.getIDByName((String) this.fields.get("country").getValue());
        if (countryID < 1) {
            this.setError(new ValidationError(null, "Invalid country selected."));
            return;
        }

        City city = this.cityFactory.getOrAdd(this.fields.get("city"), countryID, this.user.getID());
        if (city == null || city.getID() < 1) {
            this.setError(new ValidationError(null, "Unable to retrieve city information."));
            return;
        }

        Address address = this.addressFactory.getOrAdd(
                this.fields.get("streetAddress1"),
                this.fields.get("streetAddress2"),
                city.getID(),
                this.fields.get("postalCode"),
                this.fields.get("phoneNumber"),
                this.user.getID());
        if (address == null || address.getID() < 0) {
            this.setError(new ValidationError(null, "Unable to retrieve address information."));
            return;
        }

        boolean createdSuccessfully = this.customerFactory.createCustomer(
                this.fields.get("customerName"),
                this.fields.get("active"),
                address.getID(),
                this.user.getID());

        if (createdSuccessfully) {
            this.promptNotification("Customer created successfully.");
            this.closeWindow();
            return;
        }

        this.setError(new ValidationError(null, "Unable to create new customer record.\nCheck console logs."));
    }
}
