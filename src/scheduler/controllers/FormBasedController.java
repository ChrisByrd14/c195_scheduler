/**
 * Super class for controller with forms.
 * Contains helper functions for display errors, etc...
 */

package scheduler.controllers;

import java.util.HashMap;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import scheduler.FormElement;
import scheduler.ValidationError;
import scheduler.validators.Validator;
import scheduler.validators.ValidatorInterface;

abstract class FormBasedController extends BaseController {

    protected ValidatorInterface validator;
    protected HashMap<String, FormElement> fields = new HashMap<>();

    @FXML protected Button submitButton;

    abstract protected void setFields();

    /**
     * Sets the error message and style class of the offending form element
     * @param error ValidationError object containing the message and offending element
     */
    protected void setError(ValidationError error) {
        this.submitButton.setDisable(false);
        this.promptError(error.getMessage());

        Object errorElement = error.getElement();
        if (errorElement == null)
            return;

        Control styleElement = ((FormElement) errorElement).getElement();
        if (styleElement == null)
            return;

        styleElement.getStyleClass().add("error");
        this.setFocus(styleElement);
    }

    /**
     * Try to set focus to the given element
     * @param element element to try to give focus to
     */
    protected void setFocus(Node element) {
        try {
            element.requestFocus();
        } catch (Exception e) {
            // not a textfield or unable to request focus, no biggie...
        }
    }

    /**
     * Remove errors from the form and validator service
     */
    protected void clearErrors() {
        this.fields.forEach((key, value) -> {
            value.getElement().getStyleClass().remove("error");
        });
    }
}
