/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scheduler.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import scheduler.factories.CustomerFactory;
import scheduler.models.EditCustomer;
import scheduler.models.User;

public class EditCustomerSelectionController extends BaseController {
    protected User user;
    protected ObservableList<EditCustomer> customers = FXCollections.observableArrayList();
    protected CustomerFactory factory = new CustomerFactory();

    @FXML private TableView<EditCustomer> editCustomerTable = new TableView<EditCustomer>();
    @FXML private TableColumn<EditCustomer, String> customerNameColumn = new TableColumn<EditCustomer, String>();
    @FXML private TableColumn<EditCustomer, String> isActiveColumn = new TableColumn<EditCustomer, String>();
    @FXML private TableColumn<EditCustomer, String> address1Column = new TableColumn<EditCustomer, String>();
    @FXML private TableColumn<EditCustomer, String> address2Column = new TableColumn<EditCustomer, String>();
    @FXML private TableColumn<EditCustomer, String> cityColumn = new TableColumn<EditCustomer, String>();
    @FXML private TableColumn<EditCustomer, String> postalCodeColumn = new TableColumn<EditCustomer, String>();

    @FXML private TextField searchTextField;
    @FXML private Button submitButton;

    /**
     * Initialize the controller
     * @param user 
     */
    public void initData(User user) {
        this.user = user;
        this.customers = this.factory.getAllCustomers();

        FilteredList<EditCustomer> filteredList = new FilteredList<EditCustomer>(this.customers);
        this.searchTextField.textProperty().addListener((observableList, previousValue, newValue) -> {
            filteredList.setPredicate(customer -> {
                // if search value not there or is empty
                if (newValue == null || newValue.isEmpty())
                    return true;

                String filterString = newValue.toLowerCase();

                // if customer's name or address match filterString return true
                if (customer.getCustomerName().toLowerCase().contains(filterString) ||
                        customer.getAddress().toLowerCase().contains(filterString))
                    return true;

                return false;
            });
        });
        this.editCustomerTable.setItems(filteredList);
    }

    /**
     * Launch the EditCustomer view with the selected Customer record.
     * If no record is provided alert the user of the error
     */
    @FXML private void launchEditUser() {
        EditCustomer customer = this.editCustomerTable.getSelectionModel().getSelectedItem();

        if (customer == null) {
            this.showError("You must select a customer to edit.");
            return;
        }

        EditCustomerController controller = (EditCustomerController)
                this.launchView("EditCustomer", "Edit " + customer.getCustomerName());

        controller.initData(this.user, customer);
        this.closeWindow();
    }

    /**
     * Ask user for confirmation to close the window
     */
    @FXML private void promptToCloseWindow() {
        this.presentClosePrompt();
    }
}
