package scheduler.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import scheduler.AppointmentCalendar;
import scheduler.FormElement;
import scheduler.factories.AppointmentFactory;
import scheduler.factories.CustomerFactory;
import scheduler.models.EditCustomer;
import scheduler.models.User;
import scheduler.validators.AppointmentValidator;

public class AppointmentController extends FormBasedController {

    protected User user;
    protected AppointmentValidator validator;
    protected AppointmentCalendar appointmentCalendar;
    protected final CustomerFactory customerFactory = new CustomerFactory();
    protected AppointmentFactory factory;
    protected ObservableList<EditCustomer> customers;

    @FXML protected TextField titleField;
    @FXML protected TextArea description;
    @FXML protected TextField locationField;
    @FXML protected DatePicker meetingDate;
    @FXML protected TextField startTime;
    @FXML protected TextField duration;
    @FXML protected TextField customerContact;
    @FXML protected TextField url;

    @FXML protected TableView<EditCustomer> customerTable = new TableView<EditCustomer>();
    @FXML protected TableColumn<EditCustomer, String> customerNameColumn = new TableColumn<EditCustomer, String>();

    public AppointmentController() {
        this.validator = new AppointmentValidator();
        this.factory = new AppointmentFactory();
    }

    /**
     * Set all form fields to this.fields HashMap
     */
    @Override
    protected void setFields() {
        this.fields.clear();
        this.fields.put("title", new FormElement("Title", this.titleField, this.titleField.getText()));
        this.fields.put("description", new FormElement("Description", this.description, this.description.getText()));
        this.fields.put("location", new FormElement("Location", this.locationField, this.locationField.getText()));
        this.fields.put("date", new FormElement("Date", this.meetingDate, this.meetingDate.getValue()));
        this.fields.put("startTime", new FormElement("Start Time", this.startTime, this.startTime.getText()));
        this.fields.put("duration", new FormElement("Duration", this.duration, this.duration.getText()));

        this.fields.put("customer", new FormElement("Customer",
                this.customerTable, this.customerTable.getSelectionModel().getSelectedItem()));
        this.fields.put("contact", new FormElement("Customer Contact",
                this.customerContact, this.customerContact.getText()));

        this.fields.put("url", new FormElement("URL", this.url, this.url.getText()));
    }

    /**
     * Ask user for confirmation to close
     */
    @FXML protected void promptToClose() {
        this.presentClosePrompt();
    }

    /**
     * Lock submit button from being clicked again
     */
    protected void lockForm() {
        this.validator.clearError();
        this.clearErrors();
        this.setFields();
        this.submitButton.setDisable(true);
    }
}
