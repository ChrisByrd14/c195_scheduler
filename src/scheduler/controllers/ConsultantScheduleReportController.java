package scheduler.controllers;

import java.util.Calendar;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import scheduler.factories.AppointmentFactory;
import scheduler.models.Appointment;

public class ConsultantScheduleReportController extends ReportController{

    private Calendar calendar = Calendar.getInstance();
    private AppointmentFactory appointmentFactory;

    private ObservableList<Appointment> appointments = FXCollections.observableArrayList();

    @FXML private TextField searchTextField;

    @FXML private TableView<Appointment> scheduleTable = new TableView<Appointment>();
    @FXML private TableColumn<Appointment, String> consultantColumn = new TableColumn<Appointment, String>();
    @FXML private TableColumn<Appointment, String> meetingTypeColumn = new TableColumn<Appointment, String>();
    @FXML private TableColumn<Appointment, String> customerName = new TableColumn<Appointment, String>();
    @FXML private TableColumn<Appointment, Date> startTimeColumn = new TableColumn<Appointment, Date>();
    @FXML private TableColumn<Appointment, String> locationColumn = new TableColumn<Appointment, String>();

    /**
     * Initialize the controller
     */
    public void initData() {
        this.appointmentFactory = new AppointmentFactory();
        this.loadData();
    }

    /**
     * Load Consultant Schedule information from the database and display it as a filtered list to the user
     */
    private void loadData() {
        this.scheduleTable.getItems().clear();

        this.appointments = this.appointmentFactory.getAllAppointmentsOrderedByUser();

        System.out.println(this.appointments);
        FilteredList<Appointment> filteredList = new FilteredList<Appointment>(this.appointments);
        this.searchTextField.textProperty().addListener((observableList, previousValue, newValue) -> {
            filteredList.setPredicate(appointment -> {
                // if search value not there or is empty
                if (newValue == null || newValue.isEmpty())
                    return true;

                String filterString = newValue.toLowerCase();

                // if consultant's name or customer's name match filterString return true
                if (appointment.getCreatedBy().toLowerCase().contains(filterString) ||
                        appointment.getCustomerName().toLowerCase().contains(filterString))
                    return true;

                return false;
            });
        });
        this.scheduleTable.setItems(filteredList);

        this.generateFileText();
    }

    /**
     * Generate the file text that is saved if the user decides to save the report information
     */
    private void generateFileText() {
        this.fileText.setLength(0);

        this.fileText.append("\"Consultant Schedule as of ");
        this.fileText.append(this.calendar.getTime());
        this.fileText.append("\"\n");

        this.fileText.append("\"Consultant\",\"Customer\",\"Meeting Type\",\"Date & Time\",\"Location\"\n");

        this.scheduleTable.getItems().forEach(appointment -> {
            this.fileText.append("\"");
            this.fileText.append(appointment.getCreatedBy());
            this.fileText.append("\",\"");
            this.fileText.append(appointment.getCustomerName());
            this.fileText.append("\",\"");
            this.fileText.append(appointment.getTitle());
            this.fileText.append("\",\"");
            this.fileText.append(appointment.getStartDateString());
            this.fileText.append("\",\"");
            this.fileText.append(appointment.getLocation());
            this.fileText.append("\"\n");
        });
    }
}
