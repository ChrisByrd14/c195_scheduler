package scheduler.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import scheduler.CustomerStatus;
import scheduler.FormElement;
import scheduler.factories.AddressFactory;
import scheduler.factories.CityFactory;
import scheduler.factories.CountryFactory;
import scheduler.factories.CustomerFactory;
import scheduler.models.Country;
import scheduler.models.User;
import scheduler.validators.CustomerValidator;

public class CustomerController extends FormBasedController {

    protected User user;
    protected CustomerValidator validator;
    protected Country countryData;

    // Factories
    protected CustomerFactory customerFactory = new CustomerFactory();
    protected AddressFactory addressFactory = new AddressFactory();
    protected CityFactory cityFactory = new CityFactory();
    protected CountryFactory countryFactory = new CountryFactory();

    @FXML protected TextField customerName;
    @FXML protected TextField streetAddress1;
    @FXML protected TextField streetAddress2;
    @FXML protected TextField phoneNumber;
    @FXML protected TextField city;
    @FXML protected TextField postalCode;
    @FXML protected ToggleGroup activeGroup;
    @FXML protected RadioButton activeRadioButton;
    @FXML protected RadioButton inactiveRadioButton;
    @FXML protected ChoiceBox country;
    @FXML protected Button submitButton;

    public CustomerController() {
        this.validator = new CustomerValidator();
    }

    /**
     * Set up the controller for use
     * @param user object with logged-in user's information
     */
    protected void setUpCustomerController(User user) {
        this.user = user;
        this.loadStyleSheet(this.cancelButton.getScene(), "styles");

        this.activeGroup = new ToggleGroup();
        this.activeRadioButton.setToggleGroup(this.activeGroup);
        this.inactiveRadioButton.setToggleGroup(this.activeGroup);

        this.countryData = new Country();
        ObservableList<String> countries = this.countryData.getCountryNames();
        this.country.setItems(countries);
    }

    /**
     * Set all form fields on the view to the this.fields HashMap
     */
    protected void setFields() {
        this.fields.clear();
        if (this.activeRadioButton.isSelected()) {
            this.fields.put("active", new FormElement("Active radio", this.activeRadioButton, CustomerStatus.ACTIVE));
        } else {
            this.fields.put("active", new FormElement("Active radio", this.inactiveRadioButton, CustomerStatus.INACTIVE));
        }
        this.fields.put("customerName", new FormElement("Customer name", this.customerName, this.customerName.getText()));
        this.fields.put("streetAddress1", new FormElement("Street 1", this.streetAddress1, this.streetAddress1.getText()));
        this.fields.put("streetAddress2", new FormElement("Street 2", this.streetAddress2 , this.streetAddress2.getText()));
        this.fields.put("city", new FormElement("City", this.city, this.city.getText()));
        this.fields.put("postalCode", new FormElement("Postal Code", this.postalCode, this.postalCode.getText()));
        this.fields.put("phoneNumber", new FormElement("Phone number", this.postalCode, this.phoneNumber.getText()));
        this.fields.put("country", new FormElement("Country", this.country, this.country.getValue()));
    }

    /**
     * Ask user for confirmation to close the window
     */
    @FXML protected void promptToCloseWindow() {
        this.presentClosePrompt();
    }

    /**
     * Lock the form and prevent the user from clicking it again
     */
    protected void lockForm() {
        this.validator.clearError();
        this.clearErrors();
        this.setFields();
        this.submitButton.setDisable(true);
    }
}
