package scheduler.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.TimeZone;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import scheduler.AppointmentCalendar;
import scheduler.factories.CustomerFactory;
import scheduler.models.Appointment;
import scheduler.models.Customer;
import scheduler.models.User;

public class AppointmentDetailController extends BaseController {

    private Appointment appointment;
    private User user;
    private TimeZone zone;
    private AppointmentCalendar appointmentCalendar;
    private final CustomerFactory customerFactory = new CustomerFactory();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

    @FXML protected Button deleteButton;
    @FXML protected TextField titleField;
    @FXML protected Label customerName;
    @FXML protected TextArea description;
    @FXML protected TextField locationField;
    @FXML protected DatePicker meetingDate;
    @FXML protected TextField startTime;
    @FXML protected TextField endTime;
    @FXML protected TextField customerContact;
    @FXML protected TextField url;

    /**
     * Initialize the controller
     * @param appointment Appointment object to display details of
     * @param user logged in user
     * @param appointmentCalendar  calendar that may be updated, if user edits the appointment
     */
    public void initData(Appointment appointment, User user, AppointmentCalendar appointmentCalendar) {
        this.appointment = appointment;
        this.user = user;
        this.appointmentCalendar = appointmentCalendar;
        this.zone = TimeZone.getDefault();

        this.titleField.setText(this.appointment.getTitle());
        this.description.setText(this.appointment.getDescription());
        this.locationField.setText(this.appointment.getLocation());
        this.customerContact.setText(this.appointment.getContact());
        this.url.setText(this.appointment.getUrl());

        Customer customer = this.customerFactory.getCustomerByID(this.appointment.getCustomerID());
        this.customerName.setText(customer.getCustomerName());

        LocalDateTime start = this.getLocalDateTime(this.appointment.getStart());
        this.meetingDate.setValue(start.toLocalDate());
        this.startTime.setText(start.format(this.formatter));

        LocalDateTime end = this.getLocalDateTime(this.appointment.getEnd());
        this.endTime.setText(end.format(this.formatter));
    }

    private LocalDateTime getLocalDateTime(Timestamp stamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(stamp.getTime());
        calendar.add(Calendar.MILLISECOND, this.zone.getOffset(calendar.getTimeInMillis()));
        return LocalDateTime.ofInstant(calendar.toInstant(), ZoneId.systemDefault());
    }

    /**
     * Launch the EditAppointment view and close AppointmentDetail view
     */
    @FXML private void launchEditAppointment() {
        EditAppointmentController controller = (EditAppointmentController)
                this.launchView("EditAppointment", "Edit Appointment");
        controller.initData(this.user, this.appointment, this.appointmentCalendar);
        this.closeWindow();
    }

    /**
     * Confirm the user wants to delete the appointment. If yes then delete appointment
     */
    @FXML private void promptToDelete() {
        boolean really = this.promptConfirmation("Do you really want to delete this appointment?");

        if ( ! really)
            return;

        this.appointment.removeFromDatabase();
        this.appointmentCalendar.reload();
        this.closeWindow();
    }

    /**
     * Ask user for confirmation to close the window
     */
    @FXML private void promptToClose() {
        this.presentClosePrompt();
    }
}
