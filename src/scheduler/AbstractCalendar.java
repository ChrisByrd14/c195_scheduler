package scheduler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import scheduler.controllers.MainScreenController;
import scheduler.models.Appointment;
import scheduler.models.User;

abstract class AbstractCalendar {

    /**
     * Width for a day pane in the calendar
     */
    protected final int PANE_WIDTH = 100;

    /**
     * Height for a day pane in the calendar
     */
    protected final int PANE_HEIGHT = 100;

    /**
     * JavaFX Node calendar will populate
     */
    protected final VBox view;

    /**
     * User object, used when we need to reload appointments
     */
    protected final User user;

    /**
     * Controller used by appointment Panes to call AppointmentDetail view
     */
    protected final MainScreenController controller;

    /**
     * Model used to retrieve the latest list of appointments, as needed
     */
    protected final Appointment model;

    /**
     * List of user's appointments
     */
    protected ArrayList<Appointment> appointments;

    /**
     * Calendar object used for getting next/previous versions of each calendar
     */
    protected final Calendar calendar;

    protected final String[] months = {"January", "February", "March",
                                       "April", "May", "June", "July",
                                       "August", "September", "October",
                                       "November", "December"};

    protected final String[] days = {"Sunday", "Monday", "Tuesday",
                                     "Wednesday", "Thursday", "Friday", "Saturday"};

    protected AbstractCalendar(User user, MainScreenController controller) {
        this.user = user;
        this.controller = controller;
        this.calendar = Calendar.getInstance();
        this.view = new VBox();
        this.model = new Appointment();
        this.getAppointments();
    }

    /**
     * Abstract method for determining if an appointment Pane needs to be created for this date.
     * @param appointment Appointment object in question
     * @param dayNumber number of day of the month
     * @return true if appointment falls on the current day in question
     */
    abstract boolean appointmentOnDay(Appointment appointment, int dayNumber);

    /**
     * Abstract method for creating each week of the calendar.
     * @param dayNumber number of day of the month
     * @param lastDay Last day of the month
     */
    abstract void generateWeek(int dayNumber, int lastDay);

    /**
     * Add the generated Node to the VBox that we'll send to the AppointmentCalendar object
     * @param node JavaFX node
     */
    protected void addToView(Node node) {
        this.view.getChildren().add(node);
    }

    /**
     * Generate the header row for the calendar
     */
    protected void loadDays() {
        HBox box = new HBox();
        for (String day : this.days) {
            StackPane pane = this.getDayHeader(day);
            box.getChildren().add(pane);
        }
        this.addToView(box);
    }

    /**
     * Take the day name and create the StackPane used to house it.
     * Give the pane a CSS class to differentiate from other calendar cells
     * @param name Name of the day
     * @return StackPane object consisting of Label with the day name
     */
    protected StackPane getDayHeader(String name) {
        StackPane pane = new StackPane();
        pane.setPrefSize(this.PANE_WIDTH, 25.0);
        Label day = new Label(name);
        StackPane.setAlignment(day, Pos.CENTER);
        pane.getChildren().add(day);
        pane.getStyleClass().add("calendarHead");
        return pane;
    }

    /**
     * Get a StackPane for each calendar day.
     * Put the day number in the top-right corner and add any appointments to it
     * @param dayNumber Number to put in top-right corner
     * @return StackPane representing for the calendar cell
     */
    protected StackPane getDay(int dayNumber) {
        StackPane pane = new StackPane();
        pane.setPrefSize(this.PANE_WIDTH, this.PANE_HEIGHT);
        Label number = new Label("" + dayNumber);
        StackPane.setAlignment(number, Pos.TOP_RIGHT);
        pane.getChildren().add(number);
        pane = this.addAppointments(pane, dayNumber);
        pane.getStyleClass().add("day");
        return pane;
    }

    /**
     * Get an empty pane representing a calendar cell for the next or previous month
     * @return Pane object with CSS class .empty
     */
    protected Pane getEmptyPane() {
        Pane pane = new Pane();
        pane.setPrefSize(this.PANE_WIDTH, this.PANE_HEIGHT);
        pane.getStyleClass().add("empty");
        return pane;
    }

    /**
     * Clear the VBox of all children elements
     */
    protected void clearView() {
        this.view.getChildren().clear();
    }

    /**
     * Loop over this.appointments and if the appointment date is the same as the cell being
     * generated then add an appointment Pane to the calendar cell.
     * @param pane Calendar cell representing the day being generated
     * @param dayNumber number of the day of month
     * @return Finalized calendar cell
     */
    protected StackPane addAppointments(StackPane pane, int dayNumber) {
        VBox appointmentList = new VBox();
        boolean insetsApplied = false;

        if (this.appointments == null)
            return pane;

        for (Appointment appointment : this.appointments) {
            if (this.appointmentOnDay(appointment, dayNumber)) {
                Pane appointmentNode = new Pane();
                Label appointmentLabel = new Label(this.convertTime(appointment.getStart()));
                appointmentNode.getStyleClass().add("appointment");
                appointmentNode.getChildren().add(appointmentLabel);
                appointmentNode.setUserData(appointment);
                appointmentNode.setOnMouseClicked(event -> {
                    Pane node = (Pane) event.getSource();
                    this.controller.viewAppointmentDetails((Appointment) node.getUserData());
                });
                appointmentNode.setCursor(Cursor.HAND);
                appointmentList.getChildren().add(appointmentNode);

                double topMargin = 2;
                if ( ! insetsApplied) {
                    topMargin = 18;
                    insetsApplied = true;
                }
                VBox.setMargin(appointmentNode, new Insets(topMargin, 0, 0, 0));
            }
        }

        pane.getChildren().add(appointmentList);
        return pane;
    }

    /**
     * Convert UTC Timestamp to user's time-zoned time
     * @param time Start/End Timestamp of the appointment
     * @return String representing the time
     */
    private String convertTime(Timestamp time) {
        TimeZone zone = TimeZone.getDefault();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

        Calendar conversionCalendar = Calendar.getInstance();
        conversionCalendar.setTimeInMillis(time.getTime());
        conversionCalendar.add(Calendar.MILLISECOND, zone.getOffset(conversionCalendar.getTimeInMillis()));
        Date zonedDate = (Date) conversionCalendar.getTime();
        return formatter.format(zonedDate);
    }

    /**
     * Public-facing method to initiate appointment list update
     */
    public void reload() {
        this.getAppointments();
    }

    /**
     * Get an updated appointment list
     */
    protected void getAppointments() {
        this.appointments = this.model.getAllUserAppointments(this.user);
    }

}
