package scheduler;

import javafx.scene.Node;
import javafx.scene.layout.VBox;
import scheduler.controllers.MainScreenController;
import scheduler.models.User;

public class AppointmentCalendar {

    private final User user;

    /**
     * AbstractCalendar object representing a Month-view
     */
    private final MonthCalendar monthCalendar;

    /**
     * AbstractCalendar object representing a Week-view
     */
    private final WeekCalendar weekCalendar;

    /**
     * View to populated. Located in scheduler.views.MainScreen.fxml
     */
    private final VBox view;

    /**
     * Current calendar object
     */
    private ICalendar currentCalendar;

    public AppointmentCalendar(User user, MainScreenController controller, VBox view) {
        this.user = user;
        this.view = view;
        this.monthCalendar = new MonthCalendar(this.user, controller);
        this.weekCalendar = new WeekCalendar(this.user, controller);
    }

    /**
     * Change current calendar object to MonthCalendar and load the view accordingly
     */
    public void loadMonthView() {
        this.currentCalendar = this.monthCalendar;
        this.load();
    }

    /**
     * Change current calendar object to WeekCalendar and load the view accordingly
     */
    public void loadWeekView() {
        this.currentCalendar = this.weekCalendar;
        this.load();
    }

    /**
     * Get previous view from currentCalendar
     */
    public void getPrevious() {
        Node node = this.currentCalendar.getPrevious();
        this.load(node);
    }

    /**
     * Get next view from currentCalendar
     */
    public void getNext() {
        Node node = this.currentCalendar.getNext();
        this.load(node);
    }

    /**
     * Reload appointment data into each calendar then reload the view.
     */
    public void reload() {
        this.monthCalendar.reload();
        this.weekCalendar.reload();

        this.load();

        // toggle to reset style classes
        this.getNext();
        this.getPrevious();
    }

    /**
     * Clear the view and append the Node received from currentCalendar to it
     */
    private void load() {
        this.view.getChildren().clear();
        Node node = this.currentCalendar.getCalendarOfAppointments();
        this.view.getChildren().add(node);
    }

    /**
     * Clear the view and append node to it
     * @param node currentCalendar sub-view of appointments
     */
    private void load(Node node) {
        if (node == null)
            return;

        this.view.getChildren().clear();
        this.view.getChildren().add(node);
    }
}
