/**
 * Class used to hash the entered password for Log-In functionality
 */

package scheduler;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Password {

    private static final int KEY_LENGTH = 256;
    private static final int ROUNDS = 128;

    /**
     * Get the SHA256 MessageDigest. If th user doesn't have the digest on their system log it to the console
     * @return SHA256 MessageDigest on success, null on failure
     */
    private static MessageDigest setSHA256() {
        try {
           return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("SHA-256 not supported:\t" + e);
        }
        return null;
    }

    /**
     * Hash the password using the user as "salt"
     *
     * @param passwordText entered password
     * @param user entered username
     * @return String value of hashed password
     */
    public static String hashPassword(String passwordText, String user) {
        byte[] saltedPassword = (user.toLowerCase().trim() + passwordText).getBytes();

        MessageDigest digest = Password.setSHA256();

        if (digest == null)
            return null;

        digest.update(saltedPassword);
        byte[] hashedPassword = digest.digest();

        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < hashedPassword.length; i++) {
            int convertedByte = (hashedPassword[i] & 0xff); // convert byte to unsigned int
            buffer.append(Integer.toHexString(convertedByte));
        }

        return buffer.toString().substring(0, 49);
    }
}
