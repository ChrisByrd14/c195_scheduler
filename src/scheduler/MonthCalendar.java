package scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import scheduler.controllers.MainScreenController;
import scheduler.models.Appointment;
import scheduler.models.User;

public class MonthCalendar extends AbstractCalendar implements ICalendar {

    private int monthToDisplay;
    private int year;

    public MonthCalendar(User user, MainScreenController controller) {
        super(user, controller);
        this.monthToDisplay = this.calendar.get(Calendar.MONTH);
        this.year = this.calendar.get(Calendar.YEAR);
    }

    /**
     * Generate and return the Month-based calendar
     * @return VBox containing all calendar cells
     */
    public VBox getCalendarOfAppointments() {
        this.clearView();

        String month = this.months[this.monthToDisplay];
        Label monthLabel = new Label(month + " " + this.year);
        this.addToView(monthLabel);

        this.loadDays();

        this.calendar.set(Calendar.DAY_OF_MONTH, 1);
        this.calendar.set(Calendar.YEAR, this.year);
        int lastDay = this.calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        Date date = calendar.getTime();
        int firstDayIndex = date.getDay();
        int dayNumber = 1;

        HBox week = this.padMonthBeginning(firstDayIndex - 1);
        while (firstDayIndex < 7) {
            week.getChildren().add(this.getDay(dayNumber));
            dayNumber++;
            firstDayIndex++;
        }
        this.view.getChildren().add(week);

        // generate the remaining weeks of this month
        int lastDayIndex = -1;
        while (dayNumber <= lastDay) {
            this.generateWeek(dayNumber, lastDay);
            dayNumber += 7;
        }

        return this.view;
    }

    /**
     * Pad the beginning of the week with empty cells
     * @param numberOfDays number of days to pad
     * @return HBox that makes the Month's first week.
     */
    private HBox padMonthBeginning(int numberOfDays) {
        HBox week = new HBox();
        while (numberOfDays > -1) {
            Pane pane = this.getEmptyPane();
            week.getChildren().add(pane);
            numberOfDays--;
        }
        return week;
    }

    /**
     * Get the previous month
     * @return Generated month-based calendar for previous month
     */
    public VBox getPrevious() {
        if (this.monthToDisplay != 0) {
            this.monthToDisplay--;
        } else {
            this.monthToDisplay = Calendar.DECEMBER;
            this.year--;
        }
        this.calendar.set(Calendar.MONTH, this.monthToDisplay);
        return this.getCalendarOfAppointments();
    }

    /**
     * Get the next month
     * @return Generated month-based calendar for next month
     */
    public VBox getNext() {
        if (this.monthToDisplay == 11) {
            this.monthToDisplay = Calendar.JANUARY;
            this.year++;
        } else {
            this.monthToDisplay++;
        }
        this.calendar.set(Calendar.MONTH, this.monthToDisplay);
        return this.getCalendarOfAppointments();
    }

    /**
     * Convert start timestamp to zoned calendar instance, then determine
     * if the time zoned datetime is the same as the current date.
     * @param appointment Appointment object to check
     * @param dayNumber day of the month
     * @return true if the appointment start on the day in question
     */
    protected boolean appointmentOnDay(Appointment appointment, int dayNumber) {
        TimeZone zone = TimeZone.getDefault();

        Calendar conversionCalendar = Calendar.getInstance();
        conversionCalendar.setTimeInMillis(appointment.getStart().getTime());
        conversionCalendar.add(Calendar.MILLISECOND, zone.getOffset(conversionCalendar.getTimeInMillis()));
        return (conversionCalendar.get(Calendar.YEAR) == this.year &&
                conversionCalendar.get(Calendar.MONTH) == this.monthToDisplay &&
                conversionCalendar.get(Calendar.DAY_OF_MONTH) == dayNumber);
    }

    /**
     * Generate and return the next row of calendar cells
     * @param dayNumber day number of the first day (indexed at 0)
     * @param lastDay last day of the month
     */
    @Override
    protected void generateWeek(int dayNumber, int lastDay) {
        HBox week = new HBox();
        int lastDayIndex = -1;
        for (int weekDayNumber = 0; weekDayNumber < 7; weekDayNumber++) {
            if (dayNumber > lastDay) {
                lastDayIndex = weekDayNumber;
                break;
            }
            week.getChildren().add(this.getDay(dayNumber));
            dayNumber++;
        }

        if (week.getChildren().size() < 7)
            week = this.padLastWeek(week, lastDayIndex);

        this.view.getChildren().add(week);
    }

    /**
     * Pad the last week of the month with empty cells as needed
     * @param week HBox representing the row of days making up the week
     * @param lastDayIndex index of the position of the last actual day, 0-based
     * @return completed HBox with 7 children elements
     */
    private HBox padLastWeek(HBox week, int lastDayIndex) {
        while (lastDayIndex < 7) {
            week.getChildren().add(this.getEmptyPane());
            lastDayIndex++;
        }
        return week;
    }
}
