* ~~Implement LoginService and tests for LoginController.~~
* ~~Clean up LoginController.changeLanguage(), _maybe_...~~
* ~~Add to "Edit Customer"~~:
	* ~~Data validation~~,
	* ~~Inserting into DB~~.
* ~~Add "New Appointment" functionality~~:
	* ~~View/Controller~~
	* ~~Data validation~~
	* ~~Inserting into DB~~
* ~~Add "Edit Appointment" functionality~~:
	* ~~View/Controller~~
	* ~~Data validation~~
	* ~~Inserting into DB~~
* ~~Implement Calendar view of appointments belonging to the logged-in user~~
* ~~Implement a timed notification service to alert users X amount of time before their next appointment (Mostly done. Need to get the TimerTask pulling data from DB)~~
* ~~Add "mute" option to notification chime in MainScreenController~~
* ~~Add more tests and refactor~~
* ~~Add validation to ensure new appointments don't overlap pre-existing appointments.~~
