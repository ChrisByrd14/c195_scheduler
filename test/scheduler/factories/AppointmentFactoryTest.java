package scheduler.factories;

import java.time.Instant;
import java.time.LocalDate;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import scheduler.mocks.AppointmentFactoryMock;

public class AppointmentFactoryTest {

    private AppointmentFactoryMock factory;

    public AppointmentFactoryTest() {}

    @Before
    public void setUp() {
        this.factory = new AppointmentFactoryMock();
    }

    @After
    public void tearDown() {
        this.factory = null;
    }

    /**
     * Test that getStartInUTC() returns the expected value
     */
    @Test public void testGetStartTimeStampInUTC() {
        LocalDate date = LocalDate.of(2018, 2, 3);
        String time = "12:30";
        String utcDate = this.factory.getStartTimeStampInUTC(date, time);
        assertEquals("2018-02-03 18:30:00", utcDate);
    }

    @Test public void testGetDurationInUTC() {
        String startTime = "2018-02-03 18:30:00";
        String duration = "1:30";
        String utcDate = this.factory.getEndTimeInUTC(startTime, duration);
        assertEquals("2018-02-03 20:00:00", utcDate);
    }
}
