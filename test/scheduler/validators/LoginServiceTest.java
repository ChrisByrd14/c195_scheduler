package scheduler.validators;

import scheduler.validators.LoginValidator;
import java.util.HashMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import scheduler.FormElement;

public class LoginServiceTest {

    protected LoginValidator validator;
    protected HashMap<String, FormElement> fields = new HashMap<>();

    @Before
    public void setUp() {
        this.validator = new LoginValidator();
        this.fields.put("userName", new FormElement("User Name", null, "test"));
        this.fields.put("password", new FormElement("Password", null, "test"));
    }

    @After
    public void tearDown() {
        this.validator = null;
    }

    /**
     * Test that nonempty Username and Password return true
     */
    @Test public void testValidateSuccess() {
        this.validator.load(this.fields);
        assertTrue(this.validator.validate());
    }

    /**
     * Test that login credentials fail when username is not provided
     */
    @Test public void testValidateFailureDueToEmptyUserName() {
        this.validator.load(this.fields);
        this.fields.put("userName", new FormElement("User Name", null, ""));
        assertFalse(this.validator.validate());
        this.fields.put("userName", new FormElement("User Name", null, null));
        assertFalse(this.validator.validate());
    }

    /**
     * Test that login credentials fail when password is not provided
     */
    @Test public void testValidateFailureDueToEmptyPassword() {
        this.validator.load(this.fields);
        this.fields.put("password", new FormElement("Password", null, ""));
        assertFalse(this.validator.validate());
        this.fields.put("password", new FormElement("Password", null, null));
        assertFalse(this.validator.validate());
    }
}
