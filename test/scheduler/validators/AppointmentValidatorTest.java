
package scheduler.validators;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalUnit;
import java.util.HashMap;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import scheduler.FormElement;
import scheduler.ValidationError;
import scheduler.mocks.AppointmentValidatorMock;
import scheduler.models.EditCustomer;

public class AppointmentValidatorTest {

    private AppointmentValidatorMock validator;
    private HashMap<String, FormElement> fields;

    public AppointmentValidatorTest() {}

    @Before public void setUp() {
        this.validator = new AppointmentValidatorMock();
        this.fields = new HashMap<>();
        this.loadMap();
        this.validator.load(this.fields);
    }

    @After public void tearDown() {
        this.validator = null;
        this.fields = null;
    }

    /**
     * Load this.fields with sample form data
     */
    protected void loadMap() {
        this.fields.clear();
        this.fields.put("title", new FormElement("Title", null, "New Appointment Title"));
        this.fields.put("description", new FormElement("Description", null, "The meeting will take place at a time"));
        this.fields.put("location", new FormElement("Location", null, "Dallas Texas"));
        this.fields.put("date", new FormElement("Date", null, LocalDate.of(2100, Month.FEBRUARY, 5)));
        this.fields.put("startTime", new FormElement("Start Time", null, "12:00"));
        this.fields.put("duration", new FormElement("Duration", null, "1:45"));
        this.fields.put("contact", new FormElement("Customer Contact", null, "John Doe"));
        this.fields.put("url", new FormElement("URL", null, "https://www.google.com/"));
        EditCustomer customer = new EditCustomer(
                15, "Some Company", 1, 84, "123 Main", null, "(123)456-7890",
                "01928", 31, "Some City", 1, "United States", "1");
        this.fields.put("customer", new FormElement("Customer", null, customer));
    }

    /**
     * Helper function to change the value of the FormElement at the given key in the HashMap
     * @param key location in Hashmap where the desired FormElement is
     * @param newValue New value of the FormElement
     */
    protected void changeValue(String key, String newValue) {
        this.fields.get(key).setValue(newValue);
    }

    protected void checkErrorValues(String message) {
        ValidationError error = this.validator.getError();
        assertNotNull(error);
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), message);
    }

    /**
     * Test load of non-empty Map
     */
    @Test public void testLoadSuccess() {
        boolean result = this.validator.load(this.fields);
        assertTrue(result);
    }

    /**
     * Test load of empty Map
     */
    @Test public void testLoadFailure() {
        this.fields.clear();
        assertTrue(this.fields.isEmpty());
        boolean result = this.validator.load(this.fields);
        assertFalse(result);
    }

    /**
     * Tests that information w/ expected format validates and NewCustomerService returns true
     */
    @Test public void testValidateSuccess() {
        this.validator.load(this.fields);
        boolean success = this.validator.validate();
        assertTrue(success);
    }

    /**
     * Tests that validation fails because of empty title
     */
    @Test public void testFailureDueToEmptyTitle() {
        this.changeValue("title", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        this.checkErrorValues("Title cannot be empty.");
    }

    /**
     * Test that validation fails on empty description
     */
    @Test public void testFailureDueToEmptyDescription() {
        this.changeValue("description", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        this.checkErrorValues("Description cannot be empty.");
    }

    /**
     * Tests that validation fails on empty location
     */
    @Test public void testFailureDueToEmptyLocation() {
        this.changeValue("location", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        this.checkErrorValues("Location cannot be empty.");
    }

    /**
     * Tests that validation fails on empty startTime
     */
    @Test public void testFailureDueToEmptyStartTime() {
        this.changeValue("startTime", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        this.checkErrorValues("Start Time cannot be empty.");
    }

    /**
     * Tests that validation fails on empty endTime
     */
    @Test public void testFailureDueToDuration() {
        this.changeValue("duration", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        this.checkErrorValues("Duration cannot be empty.");
    }

    /**
     * Tests that validation fails on empty contact
     */
    @Test public void testFailureDueToEmptyContact() {
        this.changeValue("contact", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        this.checkErrorValues("Customer Contact cannot be empty.");
    }

    /**
     * Test that non-empty EditCustomer object validates
     */
    @Test public void testSuccessOnSelectedCustomer() {
        this.validator.load(this.fields);
        boolean success = this.validator.checkCustomerMock();
        assertTrue(success);
    }

    /**
     * Test that empty/non-existent customer fails to validate
     */
    @Test public void testFailureOnNullCustomer() {
        this.fields.put("customer", new FormElement(null, null, null));
        this.validator.load(this.fields);
        boolean success = this.validator.checkCustomerMock();
        assertFalse(success);
        this.checkErrorValues("No customer selected.");
    }

    /**
     * Test that validation fails when an empty customer is provided
     */
    @Test public void testFailureOnEmptyCustomer() {
        this.fields.put("customer", new FormElement("Customer", null, new EditCustomer()));
        this.validator.load(this.fields);
        boolean success = this.validator.checkCustomerMock();
        assertFalse(success);
        this.checkErrorValues("No customer selected.");
    }

    /**
     * Tests that validation fails on empty URL
     */
    @Test public void testFailureDueToEmptyURL() {
        this.changeValue("url", "");
        this.validator.load(this.fields);
        boolean success = this.validator.checkURLMock();
        assertFalse(success);
        this.checkErrorValues("Provided URL is invalid.");
    }

    /**
     * Test that validation fails on invalid URL
     */
    @Test public void testFailureDueToInvalidURL() {
        this.changeValue("url", "https://www.domaincom");
        this.validator.load(this.fields);
        boolean success = this.validator.checkURLMock();
        assertFalse(success);
        this.checkErrorValues("Provided URL is invalid.");
    }

    /**
     * Test that validation succeeds on valid URL
     */
    @Test public void testSuccessfulURLValidation() {
        this.validator.load(this.fields);
        boolean success = this.validator.checkURLMock();
        assertTrue(success);
    }

    /**
     * Test that the selected date is in the today or in the future
     */
    @Test public void testSuccessfulLocalDateSelection() {
        this.validator.load(this.fields);
        boolean success = this.validator.checkDateValidation();
        assertTrue(success);
    }

    @Test public void testAlternateSuccessfulLocalDateSelection() {
        this.fields.put("date", new FormElement(null, null, LocalDate.now()));
        this.changeValue("startTime", "23:59");
        this.validator.load(this.fields);
        boolean success = this.validator.checkDateValidation();
        assertTrue(success);
    }

    /**
     * Test that a date selected in the past will throw an error
     */
    @Test public void testFailedPastDate() {
        this.fields.put("date", new FormElement(null, null, LocalDate.now().minusDays(14)));
        this.validator.load(this.fields);
        boolean success = this.validator.checkDateValidation();
        assertFalse(success);
        this.checkErrorValues("Selected date cannot be in the past.");
    }

    /**
     * Test validation failure because of null date
     */
    @Test public void testFailedNullDate() {
        this.fields.put("date", new FormElement(null, null, null));
        this.validator.load(this.fields);
        boolean success = this.validator.checkDateValidation();
        assertFalse(success);
        this.checkErrorValues("You must select a meeting date.");
    }

    /**
     * Test that the provided duration is valid
     */
    @Test public void testSuccessfulDuration() {
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingDuration();
        assertTrue(success);
    }

    /**
     * Test that validation fails because of negative duration hours
     */
    @Test public void testFailedDurationBecauseNegativeHour() {
        this.changeValue("duration", "-1:00");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingDuration();
        assertFalse(success);
        this.checkErrorValues("Duration hours cannot be negative.");
    }

    /**
     * Test that validation fails because of negative duration minutes
     */
    @Test public void testFailedDurationBecauseNegativeMinutes() {
        this.changeValue("duration", "0:-25");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingDuration();
        assertFalse(success);
        this.checkErrorValues("Duration minutes cannot be negative.");
    }

    /**
     * Test that validation fails when meeting duration doesn't contain a semi-colon
     */
    @Test public void testFailedDurationBecauseInvalidData() {
        this.changeValue("duration", "abcdefg123567");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingDuration();
        assertFalse(success);
        this.checkErrorValues("Invalid data provided for meeting duration.");
    }

    /**
     * Test that validation fails when an exception occurs during Integer.parseInt
     */
    @Test public void testFailedDurationBecauseOfIntegerParseError() {
        this.changeValue("duration", "abc:123");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingDuration();
        assertFalse(success);
        this.checkErrorValues("Meeting duration is invalid.");
    }

    /**
     * Test that the provided duration is valid
     */
    @Test public void testSuccessfulStartTime() {
        boolean success = this.validator.checkMeetingStartTime();
        assertTrue(success);
    }

    /**
     * Test that validation fails when start time contains negative hour value
     */
    @Test public void testFailedStartTimeBecauseNegativeHours() {
        this.changeValue("startTime", "-1:00");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingStartTime();
        assertFalse(success);
        this.checkErrorValues("Start time hour cannot be negative.");
    }

    /**
     * Test that validation fails because start time minutes are negative
     */
    @Test public void testFailedStartTimeBecauseNegativeMinutes() {
        this.changeValue("startTime", "0:-25");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingStartTime();
        assertFalse(success);
        this.checkErrorValues("Start time minutes cannot be negative.");
    }

    /**
     * Test that validation fails when meeting duration doesn't contain a semi-colon
     */
    @Test public void testFailedStartTimeBecauseInvalidData() {
        this.changeValue("startTime", "abcdefg123567");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingStartTime();
        assertFalse(success);
        this.checkErrorValues("Invalid data provided for meeting start time.");
    }

    /**
     * Test that validation fails when an exception occurs during Integer.parseInt
     */
    @Test public void testFailedStartTimeBecauseOfIntegerParseError() {
        this.changeValue("startTime", "abc:123");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingStartTime();
        assertFalse(success);
        this.checkErrorValues("Meeting start time is invalid.");
    }

    /**
     * Test that meeting date that's during work week validates successfully
     */
    @Test public void testSuccessfulMeetingDate() {
        this.fields.get("date").setValue(LocalDate.of(2018, Month.MARCH, 8));
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingNotOnWeekendMock();
        assertTrue(success);
        assertNull(this.validator.getError());
    }

    /**
     * Test that meeting date that's during weekend fails validation
     */
    @Test public void testFailureOfMeetingDate() {
        this.fields.get("date").setValue(LocalDate.of(2018, Month.MARCH, 10));
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingNotOnWeekendMock();
        assertFalse(success);

        ValidationError error = this.validator.getError();
        assertEquals(error.getMessage(), "You cannot schedule an appointment on the weekend.");
    }

    /**
     * Test that meeting times between 8 and 5pm pass validation
     */
    @Test public void testSuccessfulMeetingStart() {
        this.fields.get("startTime").setValue("10:00");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingInBusinessHoursMock();
        assertTrue(success);
        assertNull(this.validator.getError());
    }

    @Test public void testFailureMeetingStartBecauseBeforeWorkHours() {
        this.fields.get("startTime").setValue("07:00");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingInBusinessHoursMock();
        assertFalse(success);

        ValidationError error = this.validator.getError();
        assertNotNull(error);
        assertEquals(error.getMessage(), "You cannot schedule a meeting to begin outside of 8am and 5pm.");
    }

    @Test public void testFailureMeetingStartBecauseAfterWorkHours() {
        this.fields.get("startTime").setValue("18:00");
        this.validator.load(this.fields);
        boolean success = this.validator.checkMeetingInBusinessHoursMock();
        assertFalse(success);

        ValidationError error = this.validator.getError();
        assertNotNull(error);
        assertEquals(error.getMessage(), "You cannot schedule a meeting to begin outside of 8am and 5pm.");
    }
}
