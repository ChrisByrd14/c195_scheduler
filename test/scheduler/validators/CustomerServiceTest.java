
package scheduler.validators;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import scheduler.FormElement;
import scheduler.ValidationError;
import scheduler.mocks.CustomerValidatorMock;

public class CustomerServiceTest {

    protected CustomerValidatorMock validator;
    protected Map<String, FormElement> fields;

    public CustomerServiceTest() {}

    @Before public void setUp() {
        this.validator = new CustomerValidatorMock();
        this.fields = new HashMap<>();
    }

    protected void loadMap() {
        this.fields.clear();
        this.fields.put("customerName", new FormElement("Customer Name", null, "Jane Doe"));
        this.fields.put("streetAddress1", new FormElement("Street Address 1", null, "123 Main Street"));
        this.fields.put("streetAddress2", new FormElement("Street Address 2", null, ""));
        this.fields.put("phoneNumber", new FormElement("Phone Number", null, "(903) 555-1234"));
        this.fields.put("city", new FormElement("City", null, "Dallas"));
        this.fields.put("postalCode", new FormElement("Postal Code", null, "01234"));
        this.fields.put("country", new FormElement("Country", null, "United States"));
        this.fields.put("active", new FormElement("Active radio", null, Boolean.TRUE));
    }

    protected void changeValue(String key, String newValue) {
        this.loadMap();
        this.fields.get(key).setValue(newValue);
    }

    @After public void tearDown() {
        this.validator = null;
    }

    /**
     * Test load of non-empty Map
     */
    @Test public void testLoadSuccess() {
        this.loadMap();
        boolean success = this.validator.load(this.fields);
        assert success;
    }

    /**
     * Test load of empty Map
     */
    @Test public void testLoadFailure() {
        this.fields.clear();
        boolean success = this.validator.load(this.fields);
        assert !success;
    }

    /**
     * Tests that information w/ expected format validates and NewCustomerService returns true
     */
    @Test public void testValidateSuccess() {
        this.loadMap();
        this.validator.load(this.fields);
        boolean success = this.validator.validate();
        assertTrue(success);
    }

    /**
     * Tests that validation fails because of empty customerName
     */
    @Test public void testValidateFailureDueToEmptyName() {
        this.changeValue("customerName", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), "Customer Name cannot be empty.");
    }

    /**
     * Test that validation fails on empty street address
     */
    @Test public void testValidateFailureDueToEmptyStreetAddress1() {
        this.changeValue("streetAddress1", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertEquals(error.getMessage(), "Street Address 1 cannot be empty.");
        assertTrue(error.getElement() instanceof FormElement);
    }

    /**
     * Tests that validation fails on empty city
     */
    @Test public void testValidateFailureDueToEmptyCity() {
        this.changeValue("city", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), "City cannot be empty.");
    }

    /**
     * Tests that validation fails on empty postalCode
     */
    @Test public void testValidateFailureDueToEmptyPostalCode() {
        this.changeValue("postalCode", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), "Postal Code cannot be empty.");
    }

    /**
     * Tests that validation fails on empty phoneNumber
     */
    @Test public void testValidateFailureDueToEmptyPhone() {
        this.changeValue("phoneNumber", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), "Phone Number cannot be empty.");
    }

    /**
     * Tess that validation fails on empty country
     */
    @Test public void testValidateFailureDueToEmptyCountry() {
        this.changeValue("country", "");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), "Country cannot be empty.");
    }

    /**
     * Tests that validation fails on invalid phone format
     */
    @Test public void testValidateFailureDueToInvalidPhone() {
        this.changeValue("phoneNumber", "(903) 555-123");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), "Phone number is invalid. Must contain 10 digits.");
    }

    /**
     * Tests that validation fails on invalid postal code
     */
    @Test  public void testValidateFailureDueToInvalidPostalCode() {
        this.changeValue("postalCode", "1234");
        this.validator.load(this.fields);
        assertFalse(this.validator.validate());
        ValidationError error = this.validator.getError();
        assertTrue(error.getElement() instanceof FormElement);
        assertEquals(error.getMessage(), "Postal code is invalid. Must contain only 5 digits.");
    }

    /**
     * Test that validation fails when no entries are in the provided fields HashMap
     */
    @Test public void testValidateFailureDueToEmptyMap() {
        CustomerValidatorMock mock = new CustomerValidatorMock();
        assertFalse(mock.validate());
    }

    /**
     * Test that NewCustomerService returns a ValidationError class with the given text and given element
     */
    @Test public void testGetError() {
        this.validator.setError();
        assertTrue(this.validator.getError() instanceof ValidationError);
        ValidationError error = this.validator.getError();
        assertEquals(error.getMessage(), "Test");
    }

    /**
     * Test that the validator's non-null error attribute is set to null when clearError is called
     */
    @Test public void testClearError() {
        this.validator.setError();
        assertNotNull(this.validator.getError());
        this.validator.clearError();
        assertNull(this.validator.getError());
    }
}
