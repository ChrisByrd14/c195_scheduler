package scheduler.validators;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import scheduler.FormElement;
import scheduler.ValidationError;

public class ValidatorTest {

    private Validator validator;

    @Before
    public void setUp() {
        this.validator = new Validator();
    }

    @After
    public void tearDown() {
        this.validator = null;
    }

    /**
     * Test that non-empty string returns true
     */
    @Test public void testCheckStringNotEmptySuccess() {
        boolean result = this.validator.checkStringNotEmpty("this is not empty");
        assertTrue(result);
    }

    /**
     * Test that empty string returns false
     */
    @Test public void testCheckStringNotEmptyFail() {
        boolean result = this.validator.checkStringNotEmpty("");
        assertFalse(result);
        result = this.validator.checkStringNotEmpty(null);
        assertFalse(result);
    }

    /**
     * Test that checkDigitLength return true with appropriate length string
     */
    @Test public void testCheckDigitLengthSuccess() {
        boolean result = this.validator.checkDigitLength("(123) 555-6789", 10);
        assertTrue( result);
        result = this.validator.checkDigitLength("a1b2c3d4e5", 5);
        assertTrue( result);
    }

    /**
     * Test that checkDigitLength return true with appropriate length string
     */
    @Test public void testCheckDigitLengthFail() {
        boolean result = this.validator.checkDigitLength(null, 10);
        assertFalse( result);
        result = this.validator.checkDigitLength("abcdefg", 5);
        assertFalse( result);
    }

    /**
     * Test that non-empty HashMap loads successfully
     */
    @Test public void testLoadSuccess() {
        Map<String, FormElement> fields = new HashMap<String, FormElement>();
        fields.put("test", new FormElement("", null, ""));
        boolean result = this.validator.load(fields);
        assertTrue(result);
    }

    /**
     * Test that empty HashMap fails
     */
    @Test public void testLoadFail() {
        Map<String, FormElement> fields = new HashMap<String, FormElement>();
        boolean result = this.validator.load(fields);
        assertFalse(result);
        fields = null;
        result = this.validator.load(fields);
        assertFalse(result);
    }

    /**
     * Test that valid string doesn't produce an error
     */
    @Test public void testValidateStringSuccess() {
        this.validator.validateString(new FormElement(null, null, "Valid string"));
        ValidationError error = this.validator.getError();
        assertNull(error);
    }

    /**
     * Test that validateStrings fails when null is provided
     * instead of a FormElement object, or when the value
     * of the FormElement is null or an empty string
     */
    @Test public void testValidateStringFail() {
        this.validator.validateString(null);
        ValidationError error = this.validator.getError();
        assertEquals("No element provided.", error.getMessage());
        this.validator.clearError();

        this.validator.validateString(new FormElement("Test1", null, null));
        error = this.validator.getError();
        assertEquals("Test1 cannot be empty.", error.getMessage());
        this.validator.clearError();

        this.validator.validateString(new FormElement("Test2", null, ""));
        error = this.validator.getError();
        assertEquals("Test2 cannot be empty.", error.getMessage());
    }

    /**
     * Test that a new Validation error is created and contains the expected information
     */
    @Test public void testCreateError() {
        FormElement element = new FormElement("Test", null, null);
        this.validator.createError(element, "Lorem ipsum");
        ValidationError error = this.validator.getError();
        assertSame((FormElement) error.getElement(), element);
        assertEquals("Lorem ipsum", error.getMessage());
    }

    /**
     * Test that getError returns null when no error has
     * been created, and returns a non-null error
     * object when an error has been created
     */
    @Test public void testGetError() {
        ValidationError error = this.validator.getError();
        assertNull(error);
        this.validator.createError(null, "test");
        error = this.validator.getError();
        assertNotNull(error);
    }

    /**
     * Test that clearError causes the returned error value to be null
     */
    @Test public void testClearError() {
        this.validator.createError(new FormElement(null, null, null), null);
        ValidationError error = this.validator.getError();
        assertNotNull(error);
        this.validator.clearError();
        error = this.validator.getError();
        assertNull(error);
    }
}
