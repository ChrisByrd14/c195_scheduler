package scheduler;

import org.junit.Test;
import static org.junit.Assert.*;

public class PasswordTest {

    /**
     * Test that the hashPassword returns the expected hash for test users
     */
    @Test
    public void testHashPassword() {
       assertEquals(Password.hashPassword("test", "test"), "37268335dd693145bdcdf92623ff819a64244b53de746d438");
       assertEquals(Password.hashPassword("Pa$$w0rd!", "user"), "bb8834c5b3467b48fbd24e2c436c41b6f2e18d17d7a4ff1ad");
       assertEquals(Password.hashPassword("aSimplePassword", "jdoe"), "1abccf6524d4fab394487ab8c117cac295f4975971aa21e14");
       assertEquals(Password.hashPassword("anotherSimplePassword", "test1"), "25f7ae79b23180cc966ecf23128264c84eb5f7dcb9c4204ee");
    }
}
