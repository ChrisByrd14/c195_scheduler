package scheduler.mocks;

import scheduler.ValidationError;
import scheduler.validators.CustomerValidator;

public class CustomerValidatorMock extends CustomerValidator {
    public CustomerValidatorMock() {
        super();
    }

    public void setError() {
        this.error = new ValidationError(null, "Test");
    }
}
