package scheduler.mocks;

public class RadioButtonMock {

    private boolean selected = false;

    public RadioButtonMock() {}

    public RadioButtonMock(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return this.selected;
    }
}
