package scheduler.mocks;

public class TextField {

    private String text = null;

    public TextField() {
        //
    }

    public TextField(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }
}
