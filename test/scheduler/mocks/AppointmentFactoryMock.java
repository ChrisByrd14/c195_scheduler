package scheduler.mocks;

import java.time.Instant;
import java.time.LocalDate;
import scheduler.factories.AppointmentFactory;

public class AppointmentFactoryMock extends AppointmentFactory {
    public AppointmentFactoryMock() {
        super();
    }

    public String getStartTimeStampInUTC(LocalDate localDate, String time) {
        String dateTime = this.getStartInUTC(localDate, time);
        return dateTime;
    }

    public String getEndTimeInUTC(String startTime, String duration) {
        String dateTime = this.getEndInUTC(startTime, duration);
        return dateTime;
    }
}
