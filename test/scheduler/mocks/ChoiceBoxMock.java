package scheduler.mocks;

public class ChoiceBoxMock {

    private String value;

    public ChoiceBoxMock() {}

    public ChoiceBoxMock(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
