package scheduler.mocks;

import scheduler.ValidationError;
import scheduler.validators.AppointmentValidator;

public class AppointmentValidatorMock extends AppointmentValidator {

    public AppointmentValidatorMock() {
        super();
    }

    public boolean checkCustomerMock() {
        this.checkCustomer();
        return (this.getError() == null);
    }

    public boolean checkURLMock() {
        this.checkURL();
        return (this.getError() == null);
    }

    public boolean checkDateValidation() {
        this.checkFutureDate();
        return (this.getError() == null);
    }

    public boolean checkMeetingDuration() {
        this.checkDuration();
        return (this.error == null);
    }

    public boolean checkMeetingStartTime() {
        this.checkStartTime();
        return (this.error == null);
    }

    public void setError() {
        this.error = new ValidationError(null, "Test");
    }

    public boolean checkMeetingNotOnWeekendMock() {
        this.checkMeetingNotOnWeekend();
        return (this.error == null);
    }

    public boolean checkMeetingInBusinessHoursMock() {
        this.checkMeetingInBusinessHours();
        return (this.error == null);
    }
}
