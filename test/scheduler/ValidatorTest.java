
package scheduler;

import scheduler.validators.Validator;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class ValidatorTest {

    private Validator validator;

    @Before public void setUp() {
        this.validator = new Validator();
    }

    /**
     * Test that checkStringNotEmpty returns true when the string is not null and not empty
     */
    @Test public void testCheckStringNotEmpty() {
        boolean success = this.validator.checkStringNotEmpty("this isn't empty.");
        assertTrue(success);
    }

    /**
     * Test that checkStringNotEmpty returns false when the provided string is null
     */
    @Test public void testFailureOfStringNotEmptyBecauseNull() {
        boolean success = this.validator.checkStringNotEmpty(null);
        assertFalse(success);
    }

    /**
     * Test that checkStringNotEmpty returns false when the provided string is empty
     */
    @Test public void testFailureOfStringNotEmptyBecauseEmpty() {
        boolean success = this.validator.checkStringNotEmpty("");
        assertFalse(success);
        success = this.validator.checkStringNotEmpty("   ");
        assertFalse(success);
    }

    /**
     * Test that checkDigitLength returns true when text contains n number of digits, where n is the provided length
     */
    @Test public void testCheckDigitLength() {
        boolean success = this.validator.checkDigitLength("123\\adkei245", 6);
        assertTrue(success);
        success = this.validator.checkDigitLength("01234567890", 11);
        assertTrue(success);
    }

    /**
     * Test that checkDigitLength returns false when text is empty
     */
    @Test public void testFailureOfCheckDigitLengthBecauseEmpty() {
        boolean success = this.validator.checkDigitLength("", 1);
        assertFalse(success);
         success = this.validator.checkDigitLength("adfa fasdfa fafwaef", 1);
        assertFalse(success);
    }

    /**
     * Test that checkDigitLength returns false when text is null
     */
    @Test public void testFailureOfCheckDigitLengthBecauseNull() {
        boolean success = this.validator.checkDigitLength(null, 1);
        assertFalse(success);
    }
}
